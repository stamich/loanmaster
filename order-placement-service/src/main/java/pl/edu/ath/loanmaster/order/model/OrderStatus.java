package pl.edu.ath.loanmaster.order.model;

public enum OrderStatus {
    NEW("New"),
    PLACED("Placed"),
    CONFIRMED("Confirmed"),
    REALISED("Realised");

    private final String orderStatus;

    OrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getStatusAsString() {
        return orderStatus;
    }
}
