package pl.edu.ath.loanmaster.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.ath.loanmaster.order.exception.OrderNotFoundException;
import pl.edu.ath.loanmaster.order.model.Order;
import pl.edu.ath.loanmaster.order.model.OrderStatus;
import pl.edu.ath.loanmaster.order.repository.OrderRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public List<Order> findAllByClientName(String clientName) {
        return orderRepository.findAllByClientName(clientName);
    }

    @Override
    public List<Order> findAllByOrderStatus(OrderStatus orderStatus) {
        return orderRepository.findAllByOrderStatus(orderStatus);
    }

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public Optional<Order> findById(Long id) throws OrderNotFoundException {
        return Optional.ofNullable(orderRepository.findById(id))
                .orElseThrow(() -> new OrderNotFoundException(id));
    }

    @Override
    public void create(Order order) {
        order.setCreatedAt(LocalDateTime.now());
        orderRepository.save(order);
    }

    @Override
    public void update(Long id, Order order) {
        Order entity = orderRepository.findById(id).get();
        entity.setUpdatedAt(LocalDateTime.now());
        entity.setClientName(order.getClientName());
        entity.setOrderStatus(order.getOrderStatus());
        orderRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        orderRepository.deleteById(id);
    }
}
