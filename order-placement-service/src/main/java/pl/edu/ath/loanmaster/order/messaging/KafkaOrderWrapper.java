package pl.edu.ath.loanmaster.order.messaging;

import pl.edu.ath.loanmaster.common.messaging.KafkaWrapper;
import pl.edu.ath.loanmaster.order.model.Order;
import pl.edu.ath.loanmaster.order.model.OrderStatus;

public class KafkaOrderWrapper implements KafkaWrapper {

    private final Order order;

    public KafkaOrderWrapper(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    @Override
    public String getObjectId() {
        return Long.toString(order.getId());
    }

    public void setOrderId(String id) {
        var result = Long.parseLong(id);
        this.order.setId(result);
    }

    @Override
    public String getMessage() {
        return order.getClientName();
    }

    public void setClientName(String clientName) {
        this.order.setClientName(clientName);
    }

    @Override
    public String getAdditionalData() {
        return order.getOrderStatus().getStatusAsString();
    }

    public void setOrderStatus(String status) {
        var result = OrderStatus.valueOf(status);
        this.order.setOrderStatus(result);
    }
}
