package pl.edu.ath.loanmaster.order.exception;

public class OrderNotFoundException extends Exception {

    public OrderNotFoundException(Long id) {
        super("Order with number: " + id + " was not found!");
    }
}
