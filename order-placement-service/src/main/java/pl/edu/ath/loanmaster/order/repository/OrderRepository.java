package pl.edu.ath.loanmaster.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.edu.ath.loanmaster.order.model.Order;
import pl.edu.ath.loanmaster.order.model.OrderStatus;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    
    @Query(value = "SELECT o FROM Order o WHERE o.clientName=:clientName")
    List<Order> findAllByClientName(String clientName);

    @Query(value = "SELECT o FROM Order o WHERE o.orderStatus=:orderStatus")
    List<Order> findAllByOrderStatus(OrderStatus orderStatus);

    Optional<Order> findById(Long id);
    
    List<Order> findAll();
    void deleteById(Long id);
}
