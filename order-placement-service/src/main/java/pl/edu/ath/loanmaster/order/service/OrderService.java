package pl.edu.ath.loanmaster.order.service;

import pl.edu.ath.loanmaster.order.exception.OrderNotFoundException;
import pl.edu.ath.loanmaster.order.model.Order;
import pl.edu.ath.loanmaster.order.model.OrderStatus;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    List<Order> findAll();
    List<Order> findAllByClientName(String clientName);
    List<Order> findAllByOrderStatus(OrderStatus orderStatus);
    Optional<Order> findById(Long id) throws OrderNotFoundException;
    void create(Order order);
    void update(Long id, Order order);
    void deleteById(Long id);
}
