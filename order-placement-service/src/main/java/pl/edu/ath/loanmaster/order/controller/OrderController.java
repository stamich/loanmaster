package pl.edu.ath.loanmaster.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.edu.ath.loanmaster.order.exception.OrderNotFoundException;
import pl.edu.ath.loanmaster.order.model.Order;
import pl.edu.ath.loanmaster.order.model.OrderStatus;
import pl.edu.ath.loanmaster.order.service.OrderService;

import java.util.List;

@RestController
@RequestMapping(path = "/order")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<Order>> getAll() {
        var orders = orderService.findAll();
        if (orders.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping(path = "/allByClient")
    public ResponseEntity<List<Order>> getAllByClientName(@RequestParam String clientName) {
        var orders = orderService.findAllByClientName(clientName);
        if (orders.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping(path = "/allByStatus")
    public ResponseEntity<List<Order>> getAllByOrderStatus(@RequestParam OrderStatus orderStatus) {
        var orders = orderService.findAllByOrderStatus(orderStatus);
        if (orders.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping(path = "/byId/{id}")
    public ResponseEntity<Order> getById(@PathVariable Long id) throws OrderNotFoundException {
        var order = orderService.findById(id).orElseThrow(() -> new OrderNotFoundException(id));
        if (order != null) {
            return new ResponseEntity<>(order, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(path = "/create")
    public ResponseEntity<Void> createOrder(@RequestBody Order order) {
        try {
            orderService.create(order);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(path = "/update/{id}")
    public ResponseEntity<Void> updateOrder(@PathVariable("id") Long id,
                                            @RequestBody Order order) throws OrderNotFoundException {
        var current = orderService.findById(id)
                .orElseThrow(() -> new OrderNotFoundException(id));
        if (current != null) {
            orderService.update(id, order);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(path = "/delete/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable("id") Long id) throws OrderNotFoundException {
        var current = orderService.findById(id)
                .orElseThrow(() -> new OrderNotFoundException(id));
        if (current != null) {
            orderService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.GONE);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
