package pl.edu.ath.loanmaster.order.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "`Order`")
public class Order extends AbstractOrder<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 150)
    private String clientName;

    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;
}
