package pl.edu.ath.loanmaster.tax.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.ath.loanmaster.tax.model.Tax;

import java.util.List;
import java.util.Optional;

public interface TaxRepository extends JpaRepository<Tax, Long> {

    List<Tax> findAll();
    Optional<Tax> findById(Long id);
    void deleteById(Long id);
    void delete(Tax tax);
}
