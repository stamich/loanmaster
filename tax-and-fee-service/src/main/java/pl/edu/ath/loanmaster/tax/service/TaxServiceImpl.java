package pl.edu.ath.loanmaster.tax.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.ath.loanmaster.common.exception.ObjectNotFoundException;
import pl.edu.ath.loanmaster.tax.model.Tax;
import pl.edu.ath.loanmaster.tax.repository.TaxRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaxServiceImpl implements TaxService {

    private TaxRepository taxRepository;

    @Autowired
    public TaxServiceImpl(TaxRepository taxRepository) {
        this.taxRepository = taxRepository;
    }

    @Override
    public List<Tax> findAll() {
        return taxRepository.findAll();
    }

    @Override
    public Optional<Tax> findById(Long id) throws ObjectNotFoundException {
        return Optional.ofNullable(taxRepository.findById(id))
                .orElseThrow(() -> new ObjectNotFoundException(id));
    }

    @Override
    public void createTax(Tax tax) {
        tax.setCreatedAt(LocalDateTime.now());
        taxRepository.save(tax);
    }

    @Override
    public void updateTax(Long id, Tax tax) {
        Tax entity = taxRepository.findById(id).get();
        entity.setUpdatedAt(LocalDateTime.now());
        entity.setTaxAmount(tax.getTaxAmount());
        entity.setTaxWeight(tax.getTaxWeight());
        taxRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        taxRepository.deleteById(id);
    }

    @Override
    public void delete(Tax tax) {
        taxRepository.delete(tax);
    }
}
