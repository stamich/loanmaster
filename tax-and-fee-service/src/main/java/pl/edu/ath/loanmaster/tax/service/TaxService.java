package pl.edu.ath.loanmaster.tax.service;

import pl.edu.ath.loanmaster.common.exception.ObjectNotFoundException;
import pl.edu.ath.loanmaster.tax.model.Tax;

import java.util.List;
import java.util.Optional;

public interface TaxService {
    List<Tax> findAll();
    Optional<Tax> findById(Long id) throws ObjectNotFoundException;
    void createTax(Tax tax);
    void updateTax(Long id, Tax tax);
    void deleteById(Long id);
    void delete(Tax tax);
}
