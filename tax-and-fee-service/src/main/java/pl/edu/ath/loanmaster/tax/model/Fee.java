package pl.edu.ath.loanmaster.tax.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "Fee")
public class Fee extends AbstractNumericalModel<Long> implements Comparable<Fee> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Double calculatedFeeAmount;

    @Column
    private Integer feeWeight;

    @Override
    public int compareTo(Fee fee) {
        return 0;
    }
}
