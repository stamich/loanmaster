package pl.edu.ath.loanmaster.tax.service;

import pl.edu.ath.loanmaster.common.exception.ObjectNotFoundException;
import pl.edu.ath.loanmaster.tax.model.Fee;

import java.util.List;
import java.util.Optional;

public interface FeeService {
    List<Fee> findAll();
    Optional<Fee> findById(Long id) throws ObjectNotFoundException;
    void createFee(Fee fee);
    void updateFee(Long id, Fee fee);
    void deleteById(Long id);
    void delete(Fee fee);
}
