package pl.edu.ath.loanmaster.tax.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.ath.loanmaster.common.exception.ObjectNotFoundException;
import pl.edu.ath.loanmaster.tax.model.Tax;
import pl.edu.ath.loanmaster.tax.service.TaxService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/tax")
public class TaxController {

    public TaxService taxService;

    @Autowired
    public TaxController(TaxService taxService) {
        this.taxService = taxService;
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<Tax>> getAll() {
        var result = taxService.findAll().stream()
                .sorted(Comparator.comparing(Tax::getId))
                .collect(Collectors.toList());
        if (result.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(path = "/byId/{id}")
    public ResponseEntity<Tax> getById(@PathVariable Long id) throws ObjectNotFoundException {
        var result = taxService.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(id));
        if (result == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(path = "/create")
    public ResponseEntity<Void> createTax(Tax tax) {
        try {
            taxService.createTax(tax);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PostMapping(path = "/update/{id}")
    public ResponseEntity<Void> updateTax(@PathVariable Long id, @RequestBody Tax tax) throws ObjectNotFoundException {
        var current = taxService.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(id));
        if (current == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        taxService.updateTax(id, tax);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/delete/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) throws ObjectNotFoundException {
        var current = taxService.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(id));
        if (current == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        taxService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
