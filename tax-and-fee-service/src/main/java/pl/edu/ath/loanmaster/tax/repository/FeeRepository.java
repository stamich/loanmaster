package pl.edu.ath.loanmaster.tax.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.ath.loanmaster.tax.model.Fee;

import java.util.List;
import java.util.Optional;

@Repository
public interface FeeRepository extends JpaRepository<Fee, Long> {

    List<Fee> findAll();
    Optional<Fee> findById(Long id);
    void deleteById(Long id);
    void delete(Fee fee);
}
