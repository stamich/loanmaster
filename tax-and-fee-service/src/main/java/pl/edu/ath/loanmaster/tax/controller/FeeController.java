package pl.edu.ath.loanmaster.tax.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.ath.loanmaster.common.exception.ObjectNotFoundException;
import pl.edu.ath.loanmaster.tax.model.Fee;
import pl.edu.ath.loanmaster.tax.service.FeeService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/fee")
public class FeeController {

    private final FeeService feeService;

    public FeeController(FeeService feeService) {
        this.feeService = feeService;
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<Fee>> getAll() {
        var result = feeService.findAll().stream()
                .sorted(Comparator.comparing(Fee::getId))
                .collect(Collectors.toList());
        if (result.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(path = "/byId/{id}")
    public ResponseEntity<Fee> getById(@PathVariable Long id) throws ObjectNotFoundException {
        var result = feeService.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(id));
        if (result == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(path = "/create")
    public ResponseEntity<Void> createFee(Fee fee) {
        try {
            feeService.createFee(fee);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PostMapping(path = "/update/{id}")
    public ResponseEntity<Void> updateFee(@PathVariable Long id, @RequestBody Fee fee) throws ObjectNotFoundException {
        var current = feeService.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(id));
        if (current == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        feeService.updateFee(id, fee);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/delete/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) throws ObjectNotFoundException {
        var current = feeService.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(id));
        if (current == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        feeService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.GONE);
    }
}
