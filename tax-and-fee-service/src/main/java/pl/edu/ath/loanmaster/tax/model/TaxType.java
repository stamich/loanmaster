package pl.edu.ath.loanmaster.tax.model;

public enum TaxType {
    MORTGAGE_LAW_TAX("Podatek z tyt. ust. hipoteki"),
    CIVIL_LAW_TRANSACTIONS_TAX("Podatek od czynnosci cyw.-pr.");

    private final String taxType;

    TaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getTaxTypeAsString() {
        return taxType;
    }
}
