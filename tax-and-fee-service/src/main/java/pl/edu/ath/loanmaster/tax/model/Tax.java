package pl.edu.ath.loanmaster.tax.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "Fee")
public class Tax extends AbstractNumericalModel<Long> implements Comparable<Tax> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Double taxAmount;

    @Column
    private Integer taxWeight;

    @Override
    public int compareTo(Tax tax) {
        return 0;
    }
}
