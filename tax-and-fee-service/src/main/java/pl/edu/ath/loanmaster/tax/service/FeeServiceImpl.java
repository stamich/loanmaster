package pl.edu.ath.loanmaster.tax.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.ath.loanmaster.common.exception.ObjectNotFoundException;
import pl.edu.ath.loanmaster.tax.model.Fee;
import pl.edu.ath.loanmaster.tax.repository.FeeRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class FeeServiceImpl implements FeeService {

    private final FeeRepository feeRepository;

    public FeeServiceImpl(FeeRepository feeRepository) {
        this.feeRepository = feeRepository;
    }

    @Override
    public List<Fee> findAll() {
        return feeRepository.findAll();
    }

    @Override
    public Optional<Fee> findById(Long id) throws ObjectNotFoundException {
        return Optional.ofNullable(feeRepository.findById(id))
                .orElseThrow(() -> new ObjectNotFoundException(id));
    }

    @Override
    public void createFee(Fee fee) {
        fee.setCreatedAt(LocalDateTime.now());
        feeRepository.save(fee);
    }

    @Override
    public void updateFee(Long id, Fee fee) {
        Fee entity = feeRepository.findById(id).get();
        entity.setUpdatedAt(LocalDateTime.now());
        entity.setFeeWeight(fee.getFeeWeight());
        entity.setCalculatedFeeAmount(fee.getCalculatedFeeAmount());
        feeRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        feeRepository.deleteById(id);
    }

    @Override
    public void delete(Fee fee) {
        feeRepository.delete(fee);
    }
}
