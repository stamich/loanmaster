package pl.edu.ath.loanmaster.tax.messaging;

import pl.edu.ath.loanmaster.common.messaging.KafkaWrapper;
import pl.edu.ath.loanmaster.tax.model.Tax;

public class KafkaTaxWrapper implements KafkaWrapper {

    private final Tax tax;

    public KafkaTaxWrapper(Tax tax) {
        this.tax = tax;
    }

    @Override
    public String getObjectId() {
        return Long.toString(tax.getId());
    }

    @Override
    public String getMessage() {
        return Double.toString(tax.getTaxAmount());
    }

    @Override
    public String getAdditionalData() {
        return Integer.toString(tax.getTaxWeight());
    }
}
