package pl.edu.ath.loanmaster.tax.model;

public enum FeeType {
    BANKING_FEE("Oplata bankowa"),
    LOAN_INSURANCE("Ubezp. kredytu"),
    PROVISION("Prowizja");

    private final String feeType;

    FeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getFeeTypeAsString() {
        return feeType;
    }
}
