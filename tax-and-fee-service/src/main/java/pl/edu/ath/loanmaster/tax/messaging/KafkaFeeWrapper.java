package pl.edu.ath.loanmaster.tax.messaging;

import pl.edu.ath.loanmaster.common.messaging.KafkaWrapper;
import pl.edu.ath.loanmaster.tax.model.Fee;

public class KafkaFeeWrapper implements KafkaWrapper {

    private final Fee fee;

    public KafkaFeeWrapper(Fee fee) {
        this.fee = fee;
    }

    @Override
    public String getObjectId() {
        return Long.toString(fee.getId());
    }

    public void setFeeId(String id) {
        var result = Long.parseLong(id);
        this.fee.setId(result);
    }

    @Override
    public String getMessage() {
        return Double.toString(fee.getCalculatedFeeAmount());
    }

    public void setFeeAmount(String calculatedFeeAmount) {
        var result = Double.parseDouble(calculatedFeeAmount);
        this.fee.setCalculatedFeeAmount(result);
    }

    @Override
    public String getAdditionalData() {
        return Integer.toString(fee.getFeeWeight());
    }

    public void setFeeWeight(String feeWeight) {
        var result = Integer.parseInt(feeWeight);
        this.fee.setFeeWeight(result);
    }
}
