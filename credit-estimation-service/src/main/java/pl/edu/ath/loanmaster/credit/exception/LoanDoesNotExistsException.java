package pl.edu.ath.loanmaster.credit.exception;

public class LoanDoesNotExistsException extends Exception {

    public LoanDoesNotExistsException(Long id) {
        super("Loan with id: " + id + " does not exist!");
    }
}
