package pl.edu.ath.loanmaster.credit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.ath.loanmaster.credit.exception.CreditDoesNotExistException;
import pl.edu.ath.loanmaster.credit.model.Credit;
import pl.edu.ath.loanmaster.credit.model.CreditWorthiness;
import pl.edu.ath.loanmaster.credit.repository.CreditRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CreditServiceImpl implements CreditService {

    private final CreditRepository creditRepository;

    @Autowired
    public CreditServiceImpl(CreditRepository creditRepository) {
        this.creditRepository = creditRepository;
    }

    @Override
    public List<Credit> findAll() {
        return creditRepository.findAll();
    }

    @Override
    public List<Credit> findAllByCreditWorthiness(CreditWorthiness creditWorthiness) {
        return creditRepository.findAllByCreditWorthiness(creditWorthiness);
    }

    @Override
    public List<Credit> findAllByPersonName(String personName) {
        return creditRepository.findAllByPersonName(personName);
    }

    @Override
    public Optional<Credit> findById(Long id) throws CreditDoesNotExistException {
        return Optional.ofNullable(creditRepository.findById(id))
                .orElseThrow(() -> new CreditDoesNotExistException(id));
    }

    @Override
    public void createCredit(Credit credit) {
        credit.setCreatedAt(LocalDateTime.now());
        creditRepository.save(credit);
    }

    @Override
    public void updateCredit(Long id, Credit credit) throws CreditDoesNotExistException {
        var entity = creditRepository.findById(id)
                .orElseThrow(() -> new CreditDoesNotExistException(id));

        entity.setPersonEmail(credit.getPersonEmail());
        entity.setPersonName(credit.getPersonName());
        entity.setSalary(credit.getSalary());
        entity.setCreditAmount(credit.getCreditAmount());
        entity.setAdditionalFees(credit.getAdditionalFees());
        entity.setAverageInstallment(credit.getAverageInstallment());
        entity.setMonthlyWeights(credit.getMonthlyWeights());
        entity.setStartDate(credit.getStartDate());
        entity.setEndDate(credit.getEndDate());

        entity.setUpdatedAt(LocalDateTime.now());
        creditRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) throws CreditDoesNotExistException {
        if (id != null) {
            creditRepository.deleteById(id);
        } else {
            throw new CreditDoesNotExistException(id);
        }
    }

    @Override
    public void delete(Credit credit) throws CreditDoesNotExistException {
        if (credit.getId() != null) {
            creditRepository.delete(credit);
        } else {
            throw new CreditDoesNotExistException(credit.getId());
        }
    }

    @Override
    public CreditWorthiness getWorthiness(Credit credit) {
        if (isPersonHasMoreCredits(credit)) {
            return CreditWorthiness.SMALL;
        }
        var amount = credit.getCreditAmount();
        var salary = credit.getSalary();
        var number = amount.divide(salary, RoundingMode.HALF_EVEN);
        return amount.compareTo(number) <= credit.getAverageInstallment().intValue() ?
                CreditWorthiness.HIGH : CreditWorthiness.AVERAGE;
    }

    @Override
    public Boolean isPersonHasMoreCredits(Credit credit) {
        var result = creditRepository.findAllByPersonName(credit.getPersonName());
        return result.size() > 1;
    }

    @Override
    public BigDecimal countAverageInstallment(Credit credit) {
        var numberOfMonths = countTimeToPayOff(credit);
        var creditAmount = creditRepository.findById(credit.getId())
                .get()
                .getCreditAmount();
        return creditAmount.divide(new BigDecimal(numberOfMonths), RoundingMode.HALF_EVEN);
    }

    @Override
    public BigDecimal countWeightedArithmeticAverage(Credit credit) {

        // Get average
        var average = countAverageInstallment(credit);

        // Count weights
        var weights = creditRepository.findById(credit.getId()).stream()
                .map(Credit::getMonthlyWeights)
                .mapToInt(w -> Integer.parseInt(String.valueOf(w)))
                .sum();

        return average.divide(new BigDecimal(weights), RoundingMode.HALF_EVEN);
    }

    @Override
    public BigDecimal countWeightedGeometricAverage(Credit credit) {

        // Get average
        var average = countAverageInstallment(credit);

        // Count weights
        var weights = creditRepository.findById(credit.getId()).stream()
                .map(Credit::getMonthlyWeights)
                .mapToInt(w -> Integer.parseInt(String.valueOf(w)))
                .sum();

        // Get logarithmic weights value
        var logWeights = Math.log(weights);

        // Divide both - average by logarithmic weights
        var division = average.divide(new BigDecimal(logWeights), RoundingMode.HALF_EVEN);

        return BigDecimal.valueOf(Math.exp(division.doubleValue()));
    }

    @Override
    public Long countTimeToPayOff(Credit credit) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate startDate = LocalDate.parse(credit.getStartDate().format(formatter));
        LocalDate endDate = LocalDate.parse(credit.getEndDate().format(formatter));

        return ChronoUnit.MONTHS.between(
                YearMonth.from(startDate),
                YearMonth.from(endDate));
    }
}
