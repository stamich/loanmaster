package pl.edu.ath.loanmaster.credit.service;

import org.springframework.data.jpa.repository.Query;
import pl.edu.ath.loanmaster.credit.exception.LoanDoesNotExistsException;
import pl.edu.ath.loanmaster.credit.model.Loan;
import pl.edu.ath.loanmaster.credit.model.LoanState;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface LoanService {

    List<Loan> findAll();
    List<Loan> findAllByPersonName(String personName);
    List<Loan> findAllByLoanState(LoanState loanState);
    Optional<Loan> findById(Long id) throws LoanDoesNotExistsException;
    void createLoan(Loan loan);
    void updateLoan(Long id, Loan loan) throws LoanDoesNotExistsException;
    void deleteById(Long id);
    void delete(Loan entity);

    Boolean isPersonHasMoreLoans(Loan loan);

    BigDecimal countAverageInstallment(Loan loan);
    BigDecimal countWeightedArithmeticAverage(Loan loan);
    BigDecimal countWeightedGeometricAverage(Loan loan);
    Long countTimeToPayOff(Loan loan);
}
