package pl.edu.ath.loanmaster.credit.exception;

public class CreditDoesNotExistException extends Exception {

    public CreditDoesNotExistException(Long id) {
        super("Credit with number: " + id + " does not exist.");
    }
}
