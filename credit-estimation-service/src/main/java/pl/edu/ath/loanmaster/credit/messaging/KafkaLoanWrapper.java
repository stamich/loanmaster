package pl.edu.ath.loanmaster.credit.messaging;

import pl.edu.ath.loanmaster.common.messaging.KafkaWrapper;
import pl.edu.ath.loanmaster.credit.model.Loan;

public class KafkaLoanWrapper implements KafkaWrapper {

    private final Loan loan;

    public KafkaLoanWrapper(Loan loan) {
        this.loan = loan;
    }

    @Override
    public String getObjectId() {
        return Long.toString(loan.getId());
    }

    @Override
    public String getMessage() {
        var id = loan.getId().toString();
        var person = loan.getPersonName();
        var amount = loan.getLoanAmount().toString();
        return "Loan with id: " + id + " for: " + person + " amount: " + amount + " created.";
    }

    @Override
    public String getAdditionalData() {
        var propertyValue = loan.getPropertyValue().toString();
        var averageInstallment = loan.getAverageInstallment().toString();
        var startDate = loan.getStartDate().toString();
        var endDate = loan.getEndDate().toString();
        return "Additional loan data - property value: " + propertyValue + ", installment: " + averageInstallment +
                ", start: " + startDate + ", end:" + endDate;
    }
}
