package pl.edu.ath.loanmaster.credit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.edu.ath.loanmaster.credit.model.Loan;
import pl.edu.ath.loanmaster.credit.model.LoanState;

import java.util.List;
import java.util.Optional;

public interface LoanRepository extends JpaRepository<Loan, Long> {

    List<Loan> findAll();

    @Query(value = "SELECT l FROM Loan l WHERE l.personName=:personName")
    List<Loan> findAllByPersonName(String personName);

    @Query(value = "SELECT l FROM Loan l WHERE l.loanState=:loanState")
    List<Loan> findAllByLoanState(LoanState loanState);

    Optional<Loan> findById(Long id);
    void deleteById(Long id);
    void delete(Loan entity);
}
