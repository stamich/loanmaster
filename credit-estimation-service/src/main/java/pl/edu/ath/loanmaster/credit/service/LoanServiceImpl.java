package pl.edu.ath.loanmaster.credit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.ath.loanmaster.credit.exception.LoanDoesNotExistsException;
import pl.edu.ath.loanmaster.credit.model.Loan;
import pl.edu.ath.loanmaster.credit.model.LoanState;
import pl.edu.ath.loanmaster.credit.repository.LoanRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
public class LoanServiceImpl implements LoanService {

    private final LoanRepository loanRepository;

    @Autowired
    public LoanServiceImpl(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    @Override
    public List<Loan> findAll() {
        return loanRepository.findAll();
    }

    @Override
    public List<Loan> findAllByPersonName(String personName) {
        return loanRepository.findAllByPersonName(personName);
    }

    @Override
    public List<Loan> findAllByLoanState(LoanState loanState) {
        return loanRepository.findAllByLoanState(loanState);
    }

    @Override
    public Optional<Loan> findById(Long id) throws LoanDoesNotExistsException {
        return Optional.ofNullable(loanRepository.findById(id))
                .orElseThrow(() -> new LoanDoesNotExistsException(id));
    }

    @Override
    public void createLoan(Loan loan) {
        loan.setUpdatedAt(LocalDateTime.now());
        loanRepository.save(loan);
    }

    @Override
    public void updateLoan(Long id, Loan loan) throws LoanDoesNotExistsException {
        var entity = loanRepository.findById(id)
                .orElseThrow(() -> new LoanDoesNotExistsException(id));
        entity.setPersonName(loan.getPersonName());
        entity.setPersonEmail(loan.getPersonEmail());
        entity.setSalary(loan.getSalary());
        entity.setLoanAmount(loan.getLoanAmount());
        entity.setPropertyValue(loan.getPropertyValue());
        entity.setPropertyType(loan.getPropertyType());
        entity.setAdditionalFees(loan.getAdditionalFees());
        entity.setAverageInstallment(loan.getAverageInstallment());
        entity.setStartDate(loan.getStartDate());
        entity.setEndDate(loan.getEndDate());
        entity.setUpdatedAt(LocalDateTime.now());
        loanRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        loanRepository.deleteById(id);
    }

    @Override
    public void delete(Loan entity) {
        loanRepository.delete(entity);
    }

    @Override
    public Boolean isPersonHasMoreLoans(Loan loan) {
        var result = loanRepository.findAllByPersonName(loan.getPersonName());
        return result.size() > 1;
    }

    @Override
    public BigDecimal countAverageInstallment(Loan loan) {
        var numberOfMonths = countTimeToPayOff(loan);
        var loanAmount = loanRepository.findById(loan.getId())
                .get()
                .getLoanAmount();
        return loanAmount.divide(new BigDecimal(numberOfMonths), RoundingMode.HALF_EVEN);
    }

    @Override
    public BigDecimal countWeightedArithmeticAverage(Loan loan) {

        // Get average
        var average = countAverageInstallment(loan);

        // Count weights
        var weights = loanRepository.findById(loan.getId()).stream()
                .map(Loan::getMonthlyWeights)
                .mapToInt(w -> Integer.parseInt(String.valueOf(w)))
                .sum();
        return average.divide(new BigDecimal(weights), RoundingMode.HALF_EVEN);
    }

    @Override
    public BigDecimal countWeightedGeometricAverage(Loan loan) {

        // Get average
        var average = countAverageInstallment(loan);

        // Count weights
        var weights = loanRepository.findById(loan.getId()).stream()
                .filter(Objects::nonNull)
                .map(Loan::getMonthlyWeights)
                .mapToInt(w -> Integer.parseInt(String.valueOf(w)))
                .sum();

        // Get logarithmic weights value
        var logWeights = Math.log(weights);

        // Divide both - average by logarithmic weights
        var division = average.divide(new BigDecimal(logWeights), RoundingMode.HALF_EVEN);

        return BigDecimal.valueOf(Math.exp(division.doubleValue()));
    }

    @Override
    public Long countTimeToPayOff(Loan loan) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate startDate = LocalDate.parse(loan.getStartDate().format(formatter));
        LocalDate endDate = LocalDate.parse(loan.getEndDate().format(formatter));

        return ChronoUnit.MONTHS.between(
                YearMonth.from(startDate),
                YearMonth.from(endDate));
    }
}
