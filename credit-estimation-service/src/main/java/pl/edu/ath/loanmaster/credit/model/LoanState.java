package pl.edu.ath.loanmaster.credit.model;

public enum LoanState {
    IN_PREPARATION("W przygotowaniu"),
    NEW("Nowy"),
    ACTIVE("Aktywny"),
    PAID_OFF("Splacony");

    private final String loanState;

    LoanState(String loanState) {
        this.loanState = loanState;
    }

    public String getLoanStateAsString() {
        return loanState;
    }
}
