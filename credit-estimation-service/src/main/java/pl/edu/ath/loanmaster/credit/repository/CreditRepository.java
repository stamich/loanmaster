package pl.edu.ath.loanmaster.credit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.edu.ath.loanmaster.credit.model.Credit;
import pl.edu.ath.loanmaster.credit.model.CreditWorthiness;

import java.util.List;
import java.util.Optional;

public interface CreditRepository extends JpaRepository<Credit, Long> {

    List<Credit> findAll();

    @Query(value = "SELECT c FROM Credit c WHERE c.creditWorthiness=:creditWorthiness")
    List<Credit> findAllByCreditWorthiness(CreditWorthiness creditWorthiness);

    @Query(value = "SELECT c FROM Credit c WHERE c.personName=:personName")
    List<Credit> findAllByPersonName(String personName);
    Optional<Credit> findById(Long id);
    void deleteById(Long id);
    void delete(Credit credit);
}
