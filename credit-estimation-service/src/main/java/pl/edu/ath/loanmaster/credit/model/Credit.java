package pl.edu.ath.loanmaster.credit.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@Table
public class Credit extends AbstractCreditModel<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String personName;

    @Column(nullable = false, unique = true)
    private String personEmail;

    @Column(nullable = false)
    private BigDecimal salary;

    @Column(nullable = false)
    private BigDecimal creditAmount;

    @Column(nullable = false)
    private List<BigDecimal> additionalFees;

    @Column
    private List<Integer> monthlyWeights;

    @Column
    private BigDecimal averageInstallment;

    @Column
    private LocalDate startDate;

    @Column
    private LocalDate endDate;

    @Column
    private BigDecimal timeToPayOff;

    @Enumerated(EnumType.STRING)
    private CreditWorthiness creditWorthiness;
}
