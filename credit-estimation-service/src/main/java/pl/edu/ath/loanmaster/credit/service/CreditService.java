package pl.edu.ath.loanmaster.credit.service;

import pl.edu.ath.loanmaster.credit.exception.CreditDoesNotExistException;
import pl.edu.ath.loanmaster.credit.model.Credit;
import pl.edu.ath.loanmaster.credit.model.CreditWorthiness;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface CreditService {

    List<Credit> findAll();
    List<Credit> findAllByCreditWorthiness(CreditWorthiness creditWorthiness);
    List<Credit> findAllByPersonName(String personName);
    Optional<Credit> findById(Long id) throws CreditDoesNotExistException;
    void createCredit(Credit credit);
    void updateCredit(Long id, Credit credit) throws CreditDoesNotExistException;
    void deleteById(Long id) throws CreditDoesNotExistException;
    void delete(Credit credit) throws CreditDoesNotExistException;

    CreditWorthiness getWorthiness(Credit credit);
    Boolean isPersonHasMoreCredits(Credit credit);

    BigDecimal countAverageInstallment(Credit credit);
    BigDecimal countWeightedArithmeticAverage(Credit credit);
    BigDecimal countWeightedGeometricAverage(Credit credit);
    Long countTimeToPayOff(Credit credit);
}
