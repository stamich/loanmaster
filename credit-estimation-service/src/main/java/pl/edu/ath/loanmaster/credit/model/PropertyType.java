package pl.edu.ath.loanmaster.credit.model;

public enum PropertyType {
    AGRICULTURAL("Rolnicza"),
    RESIDENTIAL("Mieszkalna"),
    COMMERCIAL("Komercyjna"),
    INDUSTRIAL("Przemyslowa"),
    MIXED_USE("Mieszana"),
    SPECIAL_USE("Specjalna");

    private final String propertyType;

    PropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyTypeAsString() {
        return propertyType;
    }
}
