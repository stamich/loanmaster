package pl.edu.ath.loanmaster.credit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.ath.loanmaster.credit.exception.CreditDoesNotExistException;
import pl.edu.ath.loanmaster.credit.model.Credit;
import pl.edu.ath.loanmaster.credit.model.CreditWorthiness;
import pl.edu.ath.loanmaster.credit.service.CreditService;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(path = "/credit")
public class CreditController {

    private final CreditService creditService;

    @Autowired
    public CreditController(CreditService creditService) {
        this.creditService = creditService;
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<Credit>> getAllCredits() {
        var credits = creditService.findAll();
        if (credits.isEmpty()) {
            return new ResponseEntity<>(credits, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(credits, HttpStatus.OK);
    }

    @GetMapping(path = "/allByClient")
    public ResponseEntity<List<Credit>> getAllByClientName(@RequestParam String personName) {
        var credits = creditService.findAllByPersonName(personName);
        if (credits.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(credits, HttpStatus.OK);
    }

    @GetMapping(path = "/allByWorthiness")
    public ResponseEntity<List<Credit>> getAllByCreditWorthiness(@RequestParam CreditWorthiness worthiness) {
        var credits = creditService.findAllByCreditWorthiness(worthiness);
        if (credits.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(credits, HttpStatus.OK);
    }

    @GetMapping(path = "/byId/{id}")
    public ResponseEntity<Credit> getOneById(@PathVariable Long id) throws CreditDoesNotExistException {
        var credit = creditService.findById(id)
                .orElseThrow(() -> new CreditDoesNotExistException(id));
        if (credit == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(credit, HttpStatus.OK);
    }

    @PostMapping(path = "/create")
    public ResponseEntity<Void> createOrder(@RequestBody Credit credit) {
        try {
            creditService.createCredit(credit);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/update/{id}")
    public ResponseEntity<Void> updateOrder(@PathVariable("id") Long id,
                                            @RequestBody Credit credit) throws CreditDoesNotExistException {
        var current = creditService.findById(id)
                .orElseThrow(() -> new CreditDoesNotExistException(id));
        if (current != null) {
            creditService.updateCredit(id, credit);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(path = "/delete/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable("id") Long id) throws CreditDoesNotExistException {
        var current = creditService.findById(id)
                .orElseThrow(() -> new CreditDoesNotExistException(id));
        if (current != null) {
            creditService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.GONE);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(path = "/delete")
    public ResponseEntity<Void> deleteCredit(@RequestBody Credit credit) throws CreditDoesNotExistException {
        var current = creditService.findById(credit.getId())
                .orElseThrow(() -> new CreditDoesNotExistException(credit.getId()));
        if (current != null) {
            creditService.delete(credit);
            return new ResponseEntity<>(HttpStatus.GONE);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/worthiness")
    public ResponseEntity<CreditWorthiness> getWorthiness(Credit credit) {
        var result = creditService.getWorthiness(credit);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @GetMapping(path = "/average")
    public ResponseEntity<BigDecimal> getAverage(Credit credit) {
        var result = creditService.countAverageInstallment(credit);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(path = "/weightedArithmetic")
    public ResponseEntity<BigDecimal> getWeightedArithmetic(Credit credit) {
        var result = creditService.countWeightedArithmeticAverage(credit);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(path = "/weightedGeometric")
    public ResponseEntity<BigDecimal> getWeightedGeometric(Credit credit) {
        var result = creditService.countWeightedGeometricAverage(credit);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(path = "/timeToPayOff")
    public ResponseEntity<Long> getTimeToPayOff(Credit credit) {
        var result = creditService.countTimeToPayOff(credit);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
