package pl.edu.ath.loanmaster.credit.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "Loan")
public class Loan extends AbstractCreditModel<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String personName;

    @Column(nullable = false, unique = true)
    private String personEmail;

    @Column(nullable = false)
    private BigDecimal salary;

    @Column(nullable = false)
    private BigDecimal loanAmount;

    @Column
    private BigDecimal propertyValue;

    @Enumerated(EnumType.STRING)
    private PropertyType propertyType;

    @Column(nullable = false)
    private List<BigDecimal> additionalFees;

    @Column
    private List<Integer> monthlyWeights;

    @Column
    private BigDecimal averageInstallment;

    @Column
    private LocalDate startDate;

    @Column
    private LocalDate endDate;

    @Column
    private BigDecimal timeToPayOff;

    @Enumerated(EnumType.STRING)
    private LoanState loanState;
}
