package pl.edu.ath.loanmaster.credit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.ath.loanmaster.credit.exception.LoanDoesNotExistsException;
import pl.edu.ath.loanmaster.credit.model.Loan;
import pl.edu.ath.loanmaster.credit.service.LoanService;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(path = "/loan")
public class LoanController {

    private final LoanService loanService;

    @Autowired
    public LoanController(LoanService loanService) {
        this.loanService = loanService;
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<Loan>> getAllLoans() {
        var loans = loanService.findAll();
        if (loans.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(loans, HttpStatus.OK);
    }

    @GetMapping(path = "/allByClient")
    public ResponseEntity<List<Loan>> getAllByClientName(@RequestParam String personName) {
        var loans = loanService.findAllByPersonName(personName);
        if (loans.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(loans, HttpStatus.OK);
    }

    @GetMapping(path = "/byId/{id}")
    public ResponseEntity<Loan> getOneById(@PathVariable Long id) throws LoanDoesNotExistsException {
        var loan = loanService.findById(id)
                .orElseThrow(() -> new LoanDoesNotExistsException(id));
        if (loan == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(loan, HttpStatus.OK);
    }

    @PostMapping(path = "/create")
    public ResponseEntity<Void> createOrder(@RequestBody Loan loan) {
        try {
            loanService.createLoan(loan);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/update/{id}")
    public ResponseEntity<Void> updateOrder(@PathVariable("id") Long id,
                                            @RequestBody Loan loan) throws LoanDoesNotExistsException {
        var current = loanService.findById(id)
                .orElseThrow(() -> new LoanDoesNotExistsException(id));
        if (current != null) {
            loanService.updateLoan(id, loan);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(path = "/delete/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable("id") Long id) throws LoanDoesNotExistsException {
        var current = loanService.findById(id)
                .orElseThrow(() -> new LoanDoesNotExistsException(id));
        if (current != null) {
            loanService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.GONE);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(path = "/delete")
    public ResponseEntity<Void> deleteCredit(@RequestBody Loan loan) throws LoanDoesNotExistsException {
        var current = loanService.findById(loan.getId())
                .orElseThrow(() -> new LoanDoesNotExistsException(loan.getId()));
        if (current != null) {
            loanService.delete(loan);
            return new ResponseEntity<>(HttpStatus.GONE);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/average")
    public ResponseEntity<BigDecimal> getAverage(Loan loan) {
        var result = loanService.countAverageInstallment(loan);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(path = "/weightedArithmetic")
    public ResponseEntity<BigDecimal> getWeightedArithmetic(Loan loan) {
        var result = loanService.countWeightedArithmeticAverage(loan);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(path = "/weightedGeometric")
    public ResponseEntity<BigDecimal> getWeightedGeometric(Loan loan) {
        var result = loanService.countWeightedGeometricAverage(loan);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(path = "/timeToPayOff")
    public ResponseEntity<Long> getTimeToPayOff(Loan loan) {
        var result = loanService.countTimeToPayOff(loan);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
