package pl.edu.ath.loanmaster.credit.model;

public enum CreditWorthiness {
    NONE("Brak"),
    SMALL("Niewielka"),
    AVERAGE("Srednia"),
    ABOVE_AVERAGE("Pow. średniej"),
    HIGH("Wysoka");

    private final String creditWorthiness;

    CreditWorthiness(String creditWorthiness) {
        this.creditWorthiness = creditWorthiness;
    }

    public String getCreditWorthinessAsString() {
        return creditWorthiness;
    }
}
