package pl.edu.ath.loanmaster.gateway.filter

import org.springframework.cloud.gateway.filter.GatewayFilter
import org.springframework.cloud.gateway.filter.GatewayFilterChain
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.server.ServerWebExchange
import pl.edu.ath.loanmaster.gateway.BaseTestSpecification
import pl.edu.ath.loanmaster.gateway.utility.JwtUtility

class JwtAuthenticationFilterSpec extends BaseTestSpecification {

    JwtUtility jwtUtility = Mock()
    GatewayFilter gatewayFilter = Mock()
    ServerWebExchange exchange = Stub()
    GatewayFilterChain chain = Spy()

    JwtAuthenticationFilter filter = new JwtAuthenticationFilter(jwtUtility)

    def "should return value" () {
        given:
        String token = "abcd"

        when:
        def response = jwtUtility.getClaims(token)

        then:
        response == null
//        given:
//        String token = "abcd"
//        exchange.getRequest() >> { callRealMethod() }
//
//        when:
//        filter.filter(exchange, chain)
//
//        then:
//        1 * jwtUtility.getClaims(token)
    }
}
