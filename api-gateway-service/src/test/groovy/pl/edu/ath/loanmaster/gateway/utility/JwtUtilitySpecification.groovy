package pl.edu.ath.loanmaster.gateway.utility

import io.jsonwebtoken.Claims
import org.springframework.beans.factory.annotation.Value
import pl.edu.ath.loanmaster.gateway.BaseTestSpecification

class JwtUtilitySpecification extends BaseTestSpecification {

    @Value('${jwt.secret}')
    String jwtSecret = "mySecret"

    Claims claims = Mock()
    JwtUtility jwtUtility = new JwtUtility()

    def "should return null value" () {

        given:
        String token = "abcd"

        when:
        def response = jwtUtility.getClaims(token)

        then:
        response == null
    }
}
