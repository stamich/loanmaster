package pl.edu.ath.loanmaster.gateway

import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import pl.edu.ath.loanmaster.gateway.filter.JwtAuthenticationFilter
import pl.edu.ath.loanmaster.gateway.utility.JwtUtility
import spock.lang.Specification

@SpringBootTest
@ContextConfiguration(loader = SpringBootContextLoader, classes = [JwtAuthenticationFilter, JwtUtility])
class BaseTestSpecification extends Specification {
}
