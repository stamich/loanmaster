package pl.edu.ath.loanmaster.gateway.exception;

import javax.naming.AuthenticationException;

/**
 * Exception handler class for Jwt malformed tokens.
 * @author Michal Stawarski
 */
public class JwtTokenMalformedException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public JwtTokenMalformedException(String message) {
		super(message);
	}

}
