package pl.edu.ath.loanmaster.gateway.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.ath.loanmaster.gateway.filter.JwtAuthenticationFilter;

/**
 * Class responsible for configuration all properties
 * of the Gateway for application microservices.
 * @author Michal Stawarski
 * @version 1.0
 */
@Configuration
@EnableHystrix
public class GatewayConfiguration {

    private final JwtAuthenticationFilter authenticationFilter;

    @Autowired
    public GatewayConfiguration(JwtAuthenticationFilter authenticationFilter) {
        this.authenticationFilter = authenticationFilter;
    }

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("security", r -> r.path("/security/**")
                        .filters(f -> f.filter(authenticationFilter))
                        .uri("lb://security-service"))
                .route("estimation", r -> r.path("/estimation/**")
                        .filters(f -> f.filter(authenticationFilter))
                        .uri("lb://estimation-service"))
                .route("order", r -> r.path("/order/**")
                        .filters(f -> f.filter(authenticationFilter))
                        .uri("lb://order-service"))
                .route("catalog", r -> r.path("/catalog/**")
                        .filters(f -> f.filter(authenticationFilter))
                        .uri("lb://catalog-service"))
                .route("taxes", r -> r.path("/taxes/**")
                        .filters(f -> f.filter(authenticationFilter))
                        .uri("lb://taxes-service"))
                .build();
    }

//    @Bean
//    public ServerCodecConfigurer serverCodecConfigurer() {
//        return ServerCodecConfigurer.create();
//    }
}
