package pl.edu.ath.loanmaster.gateway.exception;

import javax.naming.AuthenticationException;

/**
 * Exception handler class for Jwt missing tokens.
 * @author Michal Stawarski
 */
public class JwtTokenMissingException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public JwtTokenMissingException(String message) {
		super(message);
	}

}
