package pl.edu.ath.loanmaster.account.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfiguration {

    @Bean
    public Docket apiProduct() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("pl.edu.ath.loanmaster.account"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact("Michal Stawarski", "https://codeswarm.io", "m.ski@com.com");
        return new ApiInfoBuilder()
                .title("Template project")
                .description("Project for engineer degree")
                .version("Version 1.1")
                .termsOfServiceUrl("EU terms of service url")
                .license("License open")
                .licenseUrl("http://link-to-license.com")
                .contact(contact)
                .build();
    }
}
