package pl.edu.ath.loanmaster.account.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Getter
@Setter
@Entity
public class Person extends AbstractPerson<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true, length = 50)
    private String email;

    // Implemented from UserDetails
    @Column(nullable = false, unique = true, length = 50)
    private String username;

    @Column(nullable = false, unique = true, length = 100)
    private String password;

    @ElementCollection
//    @SortNatural
    @JoinTable(name = "client_type")
    @Enumerated(EnumType.STRING)
    @Column(name = "type", length = 25, nullable = false)
    private Set<ClientType> clientTypes = new TreeSet<>();

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    private List<PersonRole> roles;
//    @ElementCollection
////    @SortNatural
//    @JoinTable(name = "role")
//    @Enumerated(EnumType.STRING)
//    @Column(name = "role", length = 25, nullable = false)
//    private Set<Role> roles = new HashSet<>();

    @Column
    @OneToMany(mappedBy = "person", fetch = FetchType.EAGER)
    private List<Authority> authorities;
    
    @Column
    private Boolean accountNonExpired;

    @Column
    private Boolean accountNonLocked;

    @Column
    private Boolean credentialsNonExpired;

    @Column
    private Boolean enabled;
}
