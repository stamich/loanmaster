package pl.edu.ath.loanmaster.account.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.ath.loanmaster.account.dto.PersonDto;
import pl.edu.ath.loanmaster.account.entity.Authority;
import pl.edu.ath.loanmaster.account.entity.Person;
import pl.edu.ath.loanmaster.account.model.ClientDetails;
import pl.edu.ath.loanmaster.account.repository.PersonRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
@Transactional
public class PersonDetailsService implements UserDetailsService {

    private final PersonRepository personRepository;

    @Autowired
    public PersonDetailsService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        Supplier<UsernameNotFoundException> ex =
                () -> new UsernameNotFoundException("Client was not found!");
        Person person = personRepository.findPersonByUsername(username)
                .orElseThrow(ex);
        return new ClientDetails(person);
    }

    public PersonDto getPersonByUsername(String username) {
        Optional<Person> person = Optional.ofNullable(personRepository.findPersonByUsername(username))
                .orElseThrow();

//        List<Person> roles = personRepository.findBySpecificRoles(person.get().getRoles());
        PersonDto dto = new PersonDto();
        dto.setEmail(person.get().getEmail());
        dto.setUsername(person.get().getUsername());
        dto.setPassword(person.get().getPassword());
        dto.setRoles(person.get().getRoles());
        return dto;
    }

    public List<Person> findAll() {
        return Optional.of(personRepository.findAll())
                .map(ArrayList::new)
                .orElseThrow();
    }

    public void savePerson(PersonDto dto) {

        if (dto != null) {
            Person person = new Person();
            person.setFirstName(dto.getFirstName());
            person.setLastName(dto.getLastName());
            person.setEmail(dto.getEmail());
            person.setUsername(dto.getUsername());
            person.setPassword(dto.getPassword());
            person.setClientTypes(dto.getClientTypes());
            person.setRoles(dto.getRoles());

            person.setAuthorities(dto.getAuthorities().stream()
                    .map(au -> new SimpleGrantedAuthority(au.getAuthority()))
                    .map(au -> new Authority())
                    .collect(Collectors.toList()));

            person.setAccountNonExpired(true);
            person.setAccountNonLocked(true);
            person.setCredentialsNonExpired(true);
            person.setEnabled(true);

            person.setCreatedAt(LocalDateTime.now());
            person.setCreatedBy(dto.getUsername());
            person.setUpdatedAt(LocalDateTime.now());
            person.setUpdatedBy(dto.getUsername());
            personRepository.save(person);
        }
    }

    public void updatePerson(String username, Person person) {

        var entity = Optional.ofNullable(personRepository.findPersonByUsername(username))
                .map(p -> new Person())
                .orElseThrow();

        entity.setFirstName(person.getFirstName());
        entity.setLastName(person.getLastName());
        entity.setUsername(person.getUsername());
        entity.setEmail(person.getEmail());
        entity.setPassword(person.getPassword());
        entity.setClientTypes(person.getClientTypes());
        entity.setRoles(person.getRoles());
        entity.setAuthorities(person.getAuthorities());

        entity.setAccountNonExpired(person.getAccountNonExpired());
        entity.setAccountNonLocked(person.getAccountNonLocked());
        entity.setCredentialsNonExpired(person.getCredentialsNonExpired());
        entity.setEnabled(person.getEnabled());

        entity.setUpdatedAt(LocalDateTime.now());
        entity.setUpdatedBy(getPrincipal());

        personRepository.save(entity);
    }

    // Private method returns the principal which is username of currently logged in person.
    public String getPrincipal() {
        var principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return principal instanceof UserDetails ?
                ((UserDetails) principal).getUsername() : principal.toString();
    }
}
