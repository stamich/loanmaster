package pl.edu.ath.loanmaster.account.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import pl.edu.ath.loanmaster.account.entity.ClientType;
import pl.edu.ath.loanmaster.account.entity.PersonRole;

import java.util.*;

@Getter
@Setter
public class PersonDto {

//    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private Set<ClientType> clientTypes = new TreeSet<>();
    private List<PersonRole> roles = new ArrayList<>();
    private Collection<? extends GrantedAuthority> authorities = new ArrayList<>();
}
