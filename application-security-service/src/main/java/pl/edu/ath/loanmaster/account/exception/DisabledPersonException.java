package pl.edu.ath.loanmaster.account.exception;

public class DisabledPersonException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public DisabledPersonException(String message) {
        super(message);
    }
}
