package pl.edu.ath.loanmaster.account.entity;

public enum ClientType {
    NOT_APPLICABLE,
    INDIVIDUAL,
    SMALL_BUSINESS,
    LIMITED_PARTNERSHIP,
    JOINT_STOCK_COMPANY,
    CORPORATE
}