package pl.edu.ath.loanmaster.account.filter;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.edu.ath.loanmaster.account.dto.PersonDto;
import pl.edu.ath.loanmaster.account.exception.JwtTokenMissingException;
import pl.edu.ath.loanmaster.account.service.PersonDetailsService;
import pl.edu.ath.loanmaster.account.utility.JwtUtility;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	private final JwtUtility jwtUtility;
	private final PersonDetailsService personDetailsService;

	@Autowired
	public JwtAuthenticationFilter(JwtUtility jwtUtility, PersonDetailsService personDetailsService) {
		this.jwtUtility = jwtUtility;
		this.personDetailsService = personDetailsService;
	}

	@SneakyThrows
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		String header = request.getHeader("Authorization");

		if (header == null || !header.startsWith("Bearer")) {
			throw new JwtTokenMissingException("No JWT token found in the request headers");
		}

		String token = header.substring(7);

		// Optional - verification
		jwtUtility.validateToken(token);

		PersonDto dto = jwtUtility.getPerson(token);
//		UserVo userVo = jwtUtil.getUser(token);

		UserDetails userDetails = personDetailsService.loadUserByUsername(dto.getUsername());
//		UserDetails userDetails = personDetailsService.loadUserByUsername(userVo.getUsername());

		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
				userDetails, null, userDetails.getAuthorities());

		usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

		if (SecurityContextHolder.getContext().getAuthentication() == null) {
			SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
		}
		filterChain.doFilter(request, response);
	}
}
