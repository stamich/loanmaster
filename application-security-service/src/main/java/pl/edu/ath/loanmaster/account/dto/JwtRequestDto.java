package pl.edu.ath.loanmaster.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class JwtRequestDto {

    private String email;
    private String username;
    private String password;
}
