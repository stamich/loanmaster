package pl.edu.ath.loanmaster.account.entity;

public enum Role {
    NOT_APPLICABLE("N/A"),
    CLIENT("Client"),
    OFFICE_WORKER("Office worker"),
    FINANCIAL_ANALYST("Financial analyst"),
    DEVELOPER("Developer"),
    DBA("Database admin"),
    MANAGER("Manager"),
    CTO("CTO"),
    CEO("CEO");

    private final String role;

    Role(String role) {
        this.role = role;
    }

    public String getRoleAsString() {
        return role;
    }

    @Override
    public String toString() {
        return role;
    }
}
