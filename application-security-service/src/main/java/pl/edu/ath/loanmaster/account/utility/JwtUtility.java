package pl.edu.ath.loanmaster.account.utility;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import pl.edu.ath.loanmaster.account.dto.PersonDto;
import pl.edu.ath.loanmaster.account.entity.Role;
import pl.edu.ath.loanmaster.account.exception.JwtTokenMalformedException;
import pl.edu.ath.loanmaster.account.exception.JwtTokenMissingException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Component
public class JwtUtility {

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Value("${jwt.token.validity}")
    private long tokenValidity;

    public PersonDto getPerson(final String token) {
        try {
            Claims body = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
            PersonDto dto = new PersonDto();
            dto.setUsername(body.getSubject());

            Collection<? extends GrantedAuthority> authorities = Arrays.stream(body.get("authorities").toString().split(","))
                    .map(a -> new SimpleGrantedAuthority(Arrays.toString(Role.values())))
                    .collect(Collectors.toCollection(ArrayList::new));

            dto.setAuthorities(authorities);
            return dto;
        } catch (Exception e) {
            System.out.println(e.getMessage() + " => " + e);
        }
        return new PersonDto();
    }

    public String generateToken(PersonDto dto) {
        Claims claims = Jwts.claims().setSubject(dto.getEmail());
        claims.put("roles", dto.getAuthorities());
        long nowMillis = System.currentTimeMillis();
        long expMillis = nowMillis + tokenValidity;
        Date exp = new Date(expMillis);
        return Jwts.builder().setClaims(claims).setIssuedAt(new Date(nowMillis)).setExpiration(exp)
                .signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
    }

    public void validateToken(final String token) throws JwtTokenMalformedException, JwtTokenMissingException {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
        } catch (SignatureException ex) {
            throw new JwtTokenMalformedException("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            throw new JwtTokenMalformedException("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            throw new JwtTokenMalformedException("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            throw new JwtTokenMalformedException("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            throw new JwtTokenMissingException("JWT claims string is empty.");
        }
    }
}
