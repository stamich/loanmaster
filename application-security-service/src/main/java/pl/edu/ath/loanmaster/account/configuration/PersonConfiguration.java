package pl.edu.ath.loanmaster.account.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.ath.loanmaster.account.entity.Person;

@Configuration
public class PersonConfiguration {

    @Bean
    public Person getPerson() {
        return new Person();
    }
}
