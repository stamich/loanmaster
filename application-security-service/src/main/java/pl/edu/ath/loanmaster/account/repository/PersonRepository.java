package pl.edu.ath.loanmaster.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.edu.ath.loanmaster.account.entity.Person;
import pl.edu.ath.loanmaster.account.entity.PersonRole;
import pl.edu.ath.loanmaster.account.entity.Role;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PersonRepository extends JpaRepository<Person, Long> {

    Optional<Person> findPersonByUsername(String username);

    @Query(value = "SELECT DISTINCT p FROM Person p WHERE p.roles IN ?1")
    List<Person> findDistinctByRolesIn(Set<Role> roles);

    @Query(value = "SELECT DISTINCT p FROM Person p INNER JOIN p.roles r WHERE r.role IN :roles")
    List<Person> findBySpecificRoles(@Param("roles") List<PersonRole> roles);
}
