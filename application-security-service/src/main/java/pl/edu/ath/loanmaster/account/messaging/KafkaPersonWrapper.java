package pl.edu.ath.loanmaster.account.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.ath.loanmaster.account.entity.Person;
import pl.edu.ath.loanmaster.account.repository.PersonRepository;
import pl.edu.ath.loanmaster.account.service.PersonDetailsService;

@Component
public class KafkaPersonWrapper implements KafkaWrapper {

    private final Person person;
    private final PersonRepository repository;

    @Autowired
    public KafkaPersonWrapper(Person person, PersonRepository repository) {
        this.person = person;
        this.repository = repository;
    }

    @Override
    public String getObjectId() {
        return Long.toString(person.getId());
    }

    @Override
    public String getMessage() {
        PersonDetailsService service = new PersonDetailsService(repository);
        return service.getPrincipal();
    }

    @Override
    public String getAdditionalData() {
        return person.getEmail();
    }
}
