package pl.edu.ath.loanmaster.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import pl.edu.ath.loanmaster.account.dto.JwtRequestDto;
import pl.edu.ath.loanmaster.account.dto.JwtResponseDto;
import pl.edu.ath.loanmaster.account.dto.PersonDto;
import pl.edu.ath.loanmaster.account.entity.ClientType;
import pl.edu.ath.loanmaster.account.entity.Person;
import pl.edu.ath.loanmaster.account.entity.PersonRole;
import pl.edu.ath.loanmaster.account.entity.Role;
import pl.edu.ath.loanmaster.account.exception.DisabledPersonException;
import pl.edu.ath.loanmaster.account.exception.InvalidPersonCredentialsException;
import pl.edu.ath.loanmaster.account.service.PersonDetailsService;
import pl.edu.ath.loanmaster.account.utility.JwtUtility;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/authorization")
public class PersonController {

    @Value(value = "${kafka.topic.name}")
    private String topicName;

    @Value(value = "${kafka.topic.signed}")
    private String topicSignedIn;

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final JwtUtility jwtUtility;
    private final PersonDetailsService personDetailsService;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public PersonController(KafkaTemplate<String, String> kafkaTemplate, JwtUtility jwtUtility, PersonDetailsService personDetailsService,
                            AuthenticationManager authenticationManager) {
        this.kafkaTemplate = kafkaTemplate;
        this.jwtUtility = jwtUtility;
        this.personDetailsService = personDetailsService;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping(path = "/signin")
    public ResponseEntity<JwtResponseDto> generateJwtToken(@RequestBody JwtRequestDto request) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        } catch (DisabledException dex) {
            throw new DisabledPersonException("Person is inactive.");
        } catch (BadCredentialsException ex) {
            throw new InvalidPersonCredentialsException("Wrong credentials");
        }

        UserDetails userDetails = personDetailsService.loadUserByUsername(request.getUsername());
        var username = userDetails.getUsername();
        var password = userDetails.getPassword();
        var auth = userDetails.getAuthorities();
        var roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .map(r -> new PersonRole())
                .collect(Collectors.toList());

        PersonDto dto = new PersonDto();
        dto.setUsername(username);
        dto.setPassword(password);
        dto.setAuthorities(auth);
        dto.setRoles(roles);

        String token = jwtUtility.generateToken(dto);

        String message = "Person with username: " + dto.getUsername() + " and mail: "
                + dto.getEmail()  + " created!";
        kafkaTemplate.send(topicName, message);

        return new ResponseEntity<>(new JwtResponseDto(token), HttpStatus.OK);
    }

    @PostMapping(path = "/signup")
    public ResponseEntity<String> signup(@RequestBody PersonDto personDto) {
        PersonDto dto = personDetailsService.getPersonByUsername(personDto.getUsername());

        if (dto == null) {
            personDetailsService.savePerson(personDto);
            return new ResponseEntity<>("Person successfully registered.", HttpStatus.OK);
        }
        kafkaTemplate.send(topicName, personDetailsService.getPrincipal());
        return new ResponseEntity<>("Person already exists.", HttpStatus.CONFLICT);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<Person>> getAllPersons(Person person) {

        if (isPersonAuthorizedForRestrictedData(person)) {
            List<Person> persons = personDetailsService.findAll();

            return persons.isEmpty() ?
                    new ResponseEntity<>(HttpStatus.NO_CONTENT) :
                    new ResponseEntity<>(persons, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

//    @GetMapping(path = "/all")
//    public ResponseEntity<List<Person>> getAll(Person person) {
//
//        List<Person> persons = personDetailsService.findAll();
//
//        return persons.isEmpty() ?
//                new ResponseEntity<>(HttpStatus.NO_CONTENT) :
//                new ResponseEntity<>(persons, HttpStatus.OK);
//    }

    private boolean isPersonAuthorizedForRestrictedData(Person person) {
        return person.getRoles().stream()
                .map(PersonRole::getRole)
                .anyMatch(r -> r == Role.CEO || r == Role.CTO || r == Role.DEVELOPER || r == Role.DBA);
    }

    private boolean isPersonFromClients(Person person) {
        return person.getRoles().stream()
                .map(PersonRole::getRole)
                .anyMatch(r -> r == Role.CLIENT);
    }

    private boolean isPersonFromIndividualClients(Person person) {
        return person.getClientTypes().stream()
                .anyMatch(ct -> ct == ClientType.INDIVIDUAL);
    }
}
