package pl.edu.ath.loanmaster.account.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.edu.ath.loanmaster.account.entity.ClientType;
import pl.edu.ath.loanmaster.account.entity.Person;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class ClientDetails implements UserDetails {

    private final Person person;

    public ClientDetails(Person person) {
        this.person = person;
    }

    public Set<ClientType> getClientType() {
        return person.getClientTypes();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return person.getAuthorities().stream()
                .map(au -> new SimpleGrantedAuthority(au.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return person.getPassword();
    }

    @Override
    public String getUsername() {
        return person.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return person.getAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return person.getAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return person.getCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return person.getEnabled();
    }
}
