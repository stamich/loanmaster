package pl.edu.ath.loanmaster.account.exception;

public class InvalidPersonCredentialsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidPersonCredentialsException(String message) {
        super(message);
    }
}
