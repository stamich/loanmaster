package pl.edu.ath.loanmaster.account.service

import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.test.context.ContextConfiguration
import pl.edu.ath.loanmaster.account.repository.PersonRepository
import spock.lang.Specification

@SpringBootTest
@ContextConfiguration(classes = [PersonRepository, JpaRepository], loader = SpringBootContextLoader.class)
class BaseServiceSpecification extends Specification {

}
