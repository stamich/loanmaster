package pl.edu.ath.loanmaster.account.controller

import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import pl.edu.ath.loanmaster.account.service.PersonDetailsService
import spock.lang.Specification

@SpringBootTest
@ContextConfiguration(loader = SpringBootContextLoader, classes = [PersonDetailsService])
class BaseControllerSpecification extends Specification {

}
