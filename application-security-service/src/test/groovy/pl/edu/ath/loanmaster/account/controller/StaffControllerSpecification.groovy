package pl.edu.ath.loanmaster.account.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.servlet.config.annotation.EnableWebMvc


import spock.lang.Subject
import spock.lang.Title

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@EnableWebMvc
@AutoConfigureMockMvc
@Title("The group of tests of the staff controller layer")
class StaffControllerSpecification extends BaseControllerSpecification {

//    @Autowired
//    protected MockMvc mockMvc
//
//    StaffService staffService = Mock()
//
//    @Subject
//    StaffController staffController = new StaffController(staffService)
//
//    def staff0 = new Staff()
//    def staff1 = new Staff()
//    def staff2 = new Staff()
//    def staffList = []
//
//    def setup() {
//        mockMvc = MockMvcBuilders.standaloneSetup(staffController).build()
//        staffList += [staff0, staff1, staff2]
//    }
//
//    def "when the context is loaded then all expected beans are created" () {
//        expect: "the productController and beans are created"
//        mockMvc
//    }
//
//    def "should get all products and return http code 200" () {
//
//        given:
//        staffService.findAll() >> staffList
//
//        when:
//        def response = mockMvc.perform(get("/staff/all"))
//
//        then:
//        1 * staffService.findAll() >> staffList
//        0 * _
//        response.andExpect(status().isOk())
//    }
//
//    def "should get the parameter #param" () {
//
//        given:
//        staffService.findStaffByActivityOfAccount(true) >> staffList
//
//        when:
//        def response = mockMvc.perform(get("/staff/all/active").param("activity", "true"))
//
//        then:
//        1 * staffService.findStaffByActivityOfAccount(true) >> staffList
//        0 * _
//        response.andExpect(status().isOk())
//    }
}
