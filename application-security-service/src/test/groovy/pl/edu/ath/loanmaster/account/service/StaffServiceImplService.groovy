package pl.edu.ath.loanmaster.account.service


import spock.lang.Unroll

import java.time.LocalDateTime

class StaffServiceImplService extends BaseServiceSpecification {

//    StaffRepository repository = Mock()
//    StaffServiceImpl service = new StaffServiceImpl(repository)
//
//    Staff staff0 = new Staff()
//    Staff staff1 = new Staff()
////    Staff staff2 = new Staff(id: 5, firstName: "Name3", lastName: "Last3", email: "name3@email.com", createdAt: LocalDateTime.now(), createdBy: "admin",
////            updatedAt: LocalDateTime.now(), updatedBy: "user", password: "pass", activityOfAccount: true, expirationOfAccount: true,
////            lockingOfAccount: false, expirationOfCredentials:false, staffRoles: StaffRole.DEVELOPER as HashSet<StaffRole>)
//    List<Staff> staffList = []
//
//    def setup() {
//
//        staff0.setId(1)
//        staff0.setCreatedAt(LocalDateTime.now())
//        staff0.setCreatedBy("me")
//        staff0.setUpdatedAt(LocalDateTime.now())
//        staff0.setUpdatedBy("me")
//        staff0.setEmail("my@email.com")
//        staff0.setPassword("password")
//        staff0.setStaffRoles(StaffRole.DEVELOPER as Set<StaffRole>)
//        staff0.setFirstName("Adam")
//        staff0.setLastName("Paradise")
//        staff0.setActivityOfAccount(true)
//        staff0.setLockingOfAccount(false)
//
//        staff1.setId(2)
//        staff1.setCreatedAt(LocalDateTime.now())
//        staff1.setCreatedBy("me")
//        staff1.setUpdatedAt(LocalDateTime.now())
//        staff1.setUpdatedBy("me")
//        staff1.setEmail("my2@email.com")
//        staff1.setPassword("password")
//        staff1.setStaffRoles(StaffRole.FINANCIAL_ANALYST as Set<StaffRole>)
//        staff1.setFirstName("Eva")
//        staff1.setLastName("Paradise")
//        staff1.setActivityOfAccount(true)
//        staff1.setLockingOfAccount(false)
//
//        staffList += [staff0, staff1]
//    }
//
//    @Unroll
//    def "should call repository method from service layer" () {
//
//        when:
//        service.findAll()
//
//        then:
//        1 * repository.findAll()
//    }
//
//    @Unroll
//    def "should get all staff elements" () {
//
//        given:
//        1 * repository.findAll() >> staffList
//
//        when:
//        def response = service.findAll()
//
//        then:
//        response == staffList
//    }
//
//    def "should get element with proper id" () {
//        given:
//        1 * repository.findById(2) >> Optional.ofNullable(staff1)
//
//        when:
//        def response = service.findById(2)
//
//        then:
//        response == Optional.ofNullable(staff1)
//    }
//
//    def "should get all the elements with proper activity status" () {
//
//        given:
//        1 * repository.findStaffByActivityOfAccount(true) >> [staff0, staff1]
//
//        when:
//        def response = service.findStaffByActivityOfAccount(true)
//
//        then:
//        response == [staff0, staff1]
//
////        where:
////        staff0                             | staff1                            || result
////        staff0.setActivityOfAccount(true)  | staff1.setActivityOfAccount(true) || [staff0, staff1]
////        staff0.setActivityOfAccount(false) | staff1.setActivityOfAccount(true) || ![staff0, staff1]
//    }
//
//
//    def "should get all the elements with proper locking status" () {
//
//        given:
//        1 * repository.findStaffByLockingOfAccount(true) >> []
//
//        when:
//        def response = service.findStaffByLockingOfAccount(true)
//
//        then:
//        response == []
//    }
//
//    @Unroll
//    def "should create one staff object by id" () {
//
//        given:
//
//        StaffServiceImpl staffService = Stub(StaffServiceImpl)
//        def temp = staffService.getLoggedUsername()
//
//        and:
//        def staff2 = new Staff()
//
//        staff2.setId(3)
//        staff2.setCreatedAt(LocalDateTime.now())
//        staff2.setCreatedBy("user")
//        staff2.setUpdatedAt(LocalDateTime.now())
//        staff2.setUpdatedBy("me")
//        staff2.setEmail("my2@email.com")
//        staff2.setPassword("password")
//        staff2.setStaffRoles(StaffRole.FINANCIAL_ANALYST as Set<StaffRole>)
//        staff2.setFirstName("Eva")
//        staff2.setLastName("Paradise")
//        staff2.setActivityOfAccount(true)
//        staff2.setLockingOfAccount(false)
//
//        when:
//        service.create(staff2)
//
//        then:
//        1 * repository.save(staff2)
//    }
//
//    @Unroll
//    def "one object should exist" () {
//
//        when:
//        service.deleteStaffById(2)
//
//        then:
//        1 * repository.deleteStaffById(2)
//    }

//    @Unroll
//    def "is email unique" () {
//
//        when:
//        service.isStaffEmailUnique(1, "my@email.com") >> true
//
//        then:
//        1 * service.findStaffByEmail("my@email.com") >> true
//    }
}
