Loan master

The project of system for loan, credit and mortgage handling.

Technological stack:

Java 11     
Groovy 3.0  
Gradle 7.0

Spring Boot 2.5  
Lombok 1.18     
Hibernate 5.4.30    
Hikari 3.4.5

Oracle Database 19.3    
PostgreSQL Database 13

Apache Kafka 2.6.0  
Zookeeper 3.4.9

Spock 2.0