package pl.edu.ath.loanmaster.product.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.ath.loanmaster.product.exception.ProductIdNotFoundException;
import pl.edu.ath.loanmaster.product.model.Product;
import pl.edu.ath.loanmaster.product.model.ProductDestination;
import pl.edu.ath.loanmaster.product.model.ProductType;
import pl.edu.ath.loanmaster.product.repository.ProductRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository repository) {
        this.productRepository = repository;
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> findByCreatedBy(String createdBy) {
        return productRepository.findByCreatedBy(createdBy);
    }

    @Override
    public List<Product> findAllByActive(Boolean active) {
        return productRepository.findAllByActive(active);
    }

//    @Override
//    public List<Product> findDistinctByDestinationIn(Set<ProductDestination> productDestinations) {
//        return productRepository.findDistinctByDestinationIn(productDestinations);
//    }
//
//    @Override
//    public List<Product> findDistinctByTypeIn(Set<ProductType> productTypes) {
//        return productRepository.findDistinctByTypeIn(productTypes);
//    }

    @Override
    public Optional<Product> findById(Long id) throws ProductIdNotFoundException {
        return Optional.ofNullable(productRepository.findById(id))
                .orElseThrow(() -> new ProductIdNotFoundException(id));
//        return Optional.ofNullable(productRepository.findById(id))
//                .orElse(Optional.of(new Product()));
    }

    @Override
    public Product create(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void update(Long id, Product product) throws ProductIdNotFoundException {
        Product entity = productRepository.findById(product.getId())
                .orElseThrow(() -> new ProductIdNotFoundException(id));
        entity.setUpdatedAt(LocalDateTime.now());
        entity.setUpdatedBy(product.getUpdatedBy());
        entity.setId(product.getId());
        entity.setName(product.getName());
        entity.setDestination(product.getDestination());
        entity.setType(product.getType());
        entity.setActive(product.getActive());
        productRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        productRepository.deleteAll();
    }
}
