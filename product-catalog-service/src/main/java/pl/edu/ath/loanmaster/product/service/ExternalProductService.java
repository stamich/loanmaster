package pl.edu.ath.loanmaster.product.service;

import pl.edu.ath.loanmaster.product.model.ExternalProduct;

import java.util.List;
import java.util.Optional;

public interface ExternalProductService {

    List<ExternalProduct> findAll();
    List<ExternalProduct> findAllByActive(Boolean active);
    Optional<ExternalProduct> findById(Long Id);
    ExternalProduct create(ExternalProduct product);
    void update(Long id, ExternalProduct product);
    void deleteById(Long id);
    void deleteAll();
}
