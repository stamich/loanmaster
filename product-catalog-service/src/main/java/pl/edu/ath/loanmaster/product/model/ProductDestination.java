package pl.edu.ath.loanmaster.product.model;

public enum ProductDestination {

    CORPORATE,
    INDIVIDUAL
}
