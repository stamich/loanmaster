package pl.edu.ath.loanmaster.product.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.ath.loanmaster.product.exception.ProductIdNotFoundException;
import pl.edu.ath.loanmaster.product.model.ExternalProduct;
import pl.edu.ath.loanmaster.product.service.ExternalProductService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/externalProduct")
public class ExternalProductController {

    private final ExternalProductService externalProductService;

    public ExternalProductController(ExternalProductService externalProductService) {
        this.externalProductService = externalProductService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<ExternalProduct>> getAll() {
        List<ExternalProduct> products = externalProductService.findAll();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/all/activity")
    public ResponseEntity<List<ExternalProduct>> getAllByActivity(@RequestParam Boolean active) {
        List<ExternalProduct> products = externalProductService.findAllByActive(active);

        if (products.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/one/{id}")
    public ResponseEntity<ExternalProduct> getOneById(@PathVariable("id") Long id) throws ProductIdNotFoundException {
        Optional<ExternalProduct> product = externalProductService.findById(id);
        return new ResponseEntity<>(product
                .orElseThrow(() -> new ProductIdNotFoundException(id)), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<ExternalProduct> create(@RequestBody ExternalProduct product) {
        try {
            var entity = externalProductService.create(product);
            return new ResponseEntity<>(entity, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ExternalProduct> update(@PathVariable("id") Long id, @RequestBody ExternalProduct product) {
        externalProductService.update(id, product);
        return new ResponseEntity<>(externalProductService.findById(id).get(), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable("id") Long id) {
        try {
            externalProductService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/all")
    public ResponseEntity<Void> deleteAll() {
        List<ExternalProduct> products = externalProductService.findAll();

        if (products.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        externalProductService.deleteAll();
        return new ResponseEntity<>(HttpStatus.GONE);
    }
}
