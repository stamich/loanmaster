package pl.edu.ath.loanmaster.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.edu.ath.loanmaster.product.model.Product;
import pl.edu.ath.loanmaster.product.model.ProductDestination;
import pl.edu.ath.loanmaster.product.model.ProductType;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findAll();
    List<Product> findByCreatedBy(String createdBy);
    List<Product> findAllByActive(Boolean active);

//    @Query(value = "SELECT DISTINCT p FROM Product p WHERE p.destination in ?1")
//    List<Product> findDistinctByDestinationIn(Set<ProductDestination> productDestinations);
//
//    @Query(value = "SELECT DISTINCT p FROM Product p WHERE p.type in ?1")
//    List<Product> findDistinctByTypeIn(Set<ProductType> productTypes);

    Optional<Product> findById(Long id);
    void deleteById(Long id);
    void deleteAll();
}
