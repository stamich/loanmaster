package pl.edu.ath.loanmaster.product.exception;

public class ProductIdNotFoundException extends Exception {

    public ProductIdNotFoundException(Long id) {
        super("Product with id: " + id + " not found!");
    }
}
