package pl.edu.ath.loanmaster.product.service;

import org.springframework.stereotype.Service;
import pl.edu.ath.loanmaster.product.model.ExternalProduct;
import pl.edu.ath.loanmaster.product.repository.ExternalProductRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ExternalProductServiceImpl implements ExternalProductService {

    private final ExternalProductRepository externalProductRepository;

    public ExternalProductServiceImpl(ExternalProductRepository externalProductRepository) {
        this.externalProductRepository = externalProductRepository;
    }

    @Override
    public List<ExternalProduct> findAll() {
        return externalProductRepository.findAll();
    }

    @Override
    public List<ExternalProduct> findAllByActive(Boolean active) {
        return externalProductRepository.findAllByActive(active);
    }

    @Override
    public Optional<ExternalProduct> findById(Long id) {
        return Optional.ofNullable(externalProductRepository.findById(id))
                .orElse(Optional.of(new ExternalProduct()));
    }

    @Override
    public ExternalProduct create(ExternalProduct product) {
        return externalProductRepository.save(product);
    }

    @Override
    public void update(Long id, ExternalProduct product) {
        Optional<ExternalProduct> entity = externalProductRepository.findById(id);
        entity.get().setUpdatedAt(LocalDateTime.now());
        entity.get().setId(product.getId());
        entity.get().setName(product.getName());
        entity.get().setBankName(product.getBankName());
        entity.get().setExpirationDate(product.getExpirationDate());
        entity.get().setActive(product.getActive());
        externalProductRepository.save(entity.get());
    }

    @Override
    public void deleteById(Long id) {
        externalProductRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        externalProductRepository.deleteAll();
    }
}
