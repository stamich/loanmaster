package pl.edu.ath.loanmaster.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.ath.loanmaster.product.model.ExternalProduct;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExternalProductRepository extends JpaRepository<ExternalProduct, Long> {

    List<ExternalProduct> findAll();
    List<ExternalProduct> findAllByActive(Boolean active);
    Optional<ExternalProduct> findById(Long id);
    void deleteById(Long id);
    void deleteAll();
}
