package pl.edu.ath.loanmaster.product.service;

import pl.edu.ath.loanmaster.product.exception.ProductIdNotFoundException;
import pl.edu.ath.loanmaster.product.model.Product;
import pl.edu.ath.loanmaster.product.model.ProductDestination;
import pl.edu.ath.loanmaster.product.model.ProductType;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProductService {

    List<Product> findAll();
    List<Product> findByCreatedBy(String createdBy);
    List<Product> findAllByActive(Boolean active);
//    List<Product> findDistinctByDestinationIn(Set<ProductDestination> productDestinations);
//    List<Product> findDistinctByTypeIn(Set<ProductType> productTypes);
    Optional<Product> findById(Long id) throws ProductIdNotFoundException;
    Product create(Product product);
    void update(Long id, Product product) throws ProductIdNotFoundException;
    void deleteById(Long id);
    void deleteAll();
}
