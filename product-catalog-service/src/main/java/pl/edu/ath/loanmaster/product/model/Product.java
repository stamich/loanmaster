package pl.edu.ath.loanmaster.product.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;
import java.util.TreeSet;

@Data
@Entity
@Table(name = "product")
@EqualsAndHashCode(callSuper = false)
@ToString
public class Product extends AbstractDomainModel<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 100)
    private String name;

    @ElementCollection
    //@SortNatural
    @JoinTable(name = "product_destination")
    @Enumerated(EnumType.STRING)
    @Column(name = "destination", length = 25, nullable = false)
    private Set<ProductDestination> destination = new TreeSet<>();

    @ElementCollection
    //@SortNatural
    @JoinTable(name = "product_type")
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private Set<ProductType> type = new TreeSet<>();

    @Column(nullable = false)
    private Boolean active;

}
