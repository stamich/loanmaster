package pl.edu.ath.loanmaster.product.model;

public enum ProductType {

    MORTGAGE,
    PERSONAL_LOAN,
    STUDENT_LOAN
}
