package pl.edu.ath.loanmaster.product.controller

import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import pl.edu.ath.loanmaster.product.service.ExternalProductService
import pl.edu.ath.loanmaster.product.service.ProductService
import spock.lang.Specification

@SpringBootTest
@ContextConfiguration(loader = SpringBootContextLoader, classes = [ExternalProductService, ProductService])
class BaseControllerSpecification extends Specification {

}
