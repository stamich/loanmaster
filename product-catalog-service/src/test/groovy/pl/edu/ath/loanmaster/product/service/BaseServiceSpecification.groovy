package pl.edu.ath.loanmaster.product.service

import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.test.context.ContextConfiguration
import pl.edu.ath.loanmaster.product.repository.ExternalProductRepository
import pl.edu.ath.loanmaster.product.repository.ProductRepository
import spock.lang.Specification

@SpringBootTest
@ContextConfiguration(loader = SpringBootContextLoader, classes = [JpaRepository, ExternalProductRepository, ProductRepository])
class BaseServiceSpecification extends Specification {

}
