package pl.edu.ath.loanmaster.product.service


import pl.edu.ath.loanmaster.product.model.ExternalProduct
import pl.edu.ath.loanmaster.product.repository.ExternalProductRepository
import spock.lang.Subject
import spock.lang.Unroll

import java.time.LocalDate
import java.time.LocalDateTime

class ExternalProductServiceSpecification extends BaseServiceSpecification {

    ExternalProductRepository productRepository = Mock()

    @Subject
    ExternalProductServiceImpl productService = new ExternalProductServiceImpl(productRepository)

    def ep0 = new ExternalProduct()
    def ep1 = new ExternalProduct()
    def ep2 = new ExternalProduct()
    def epList = []

    def setup() {
        ep0.setId(1)
        ep0.setCreatedAt(LocalDateTime.now())
        ep0.setCreatedBy("admin")
        ep0.setUpdatedAt(LocalDateTime.now())
        ep0.setUpdatedBy("user")
        ep0.setName("External name 1")
        ep0.setBankName("External Bank corp.")
        ep0.setExpirationDate(LocalDate.of(2021, 12, 31))
        ep0.setActive(true)

        ep1.setId(2)
        ep1.setCreatedAt(LocalDateTime.now())
        ep1.setCreatedBy("user")
        ep1.setUpdatedAt(LocalDateTime.now())
        ep1.setUpdatedBy("user")
        ep1.setName("External name 2")
        ep1.setBankName("International Banking corp.")
        ep1.setExpirationDate(LocalDate.of(2022, 06, 30))
        ep1.setActive(true)

        ep2.setId(3)
        ep2.setCreatedAt(LocalDateTime.now())
        ep2.setCreatedBy("admin")
        ep2.setUpdatedAt(LocalDateTime.now())
        ep2.setUpdatedBy("computer")
        ep2.setName("External name 3")
        ep2.setBankName("International Banking corp. no. 2")
        ep2.setExpirationDate(LocalDate.of(2022, 12, 31))
        ep2.setActive(false)

        epList += [ep0, ep1, ep2]
    }

    @Unroll
    def "should call once findAll() method from the repository" () {

        when:
        productService.findAll()

        then:
        1 * productRepository.findAll()
    }

    @Unroll
    def "should call once findAll() method and return the list of elements" () {

        given:
        1 * productRepository.findAll() >> epList

        when:
        def response = productService.findAll()

        then:
        response == epList
    }

    @Unroll
    def "should find all active products" () {

        given:
        1 * productRepository.findAllByActive(true) >> [ep0, ep1]

        when:
        def response = productService.findAllByActive(true)

        then:
        response != [ep0, ep1, ep2]
        response == [ep0, ep1]
    }

    @Unroll
    def "should find all product by its id" () {

        given:
        1 * productRepository.findById(3) >> Optional.ofNullable(ep2)

        when:
        def response = productService.findById(3)

        then:
        response == Optional.ofNullable(ep2)
    }

    @Unroll("The creating of new product #ep3")
    def "should add the element #ep3" () {

        given:
        def ep3 = new ExternalProduct()
        ep3.setId(4)
        ep3.setCreatedAt(LocalDateTime.now())
        ep3.setCreatedBy("admin")
        ep3.setUpdatedAt(LocalDateTime.now())
        ep3.setUpdatedBy("computer")
        ep3.setName("External name 4")
        ep3.setBankName("International Banking corp. no. 2")
        ep3.setExpirationDate(LocalDate.of(2022, 12, 31))
        ep3.setActive(false)

        when:
        productService.create(ep3)

        then:
        1 * productRepository.save(ep3)
    }

    @Unroll
    def "should update existing product #p2" () {

        given:
        productRepository.findById(3) >> Optional.ofNullable(ep2)
        productRepository.save(ep2) >> ep2

        when:
        productService.update(3, ep2)

        then:
        productRepository.findById(3)
        ep2.setName("Name updated")
        productRepository.save(ep2)
        ep2.getName() == "Name updated"
    }

    @Unroll
    def "should be called once deleteById() from repository" () {

        when:
        productService.deleteById(2)

        then:
        1 * productRepository.deleteById(2)
    }

    def "should be called once deleteAll() from repository" () {

        when:
        productService.deleteAll()

        then:
        1 * productRepository.deleteAll()
    }
}
