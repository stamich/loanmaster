package pl.edu.ath.loanmaster.product.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.databind.SerializationFeature
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import pl.edu.ath.loanmaster.product.model.Product
import pl.edu.ath.loanmaster.product.service.ProductService
import spock.lang.Subject

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@EnableWebMvc
@AutoConfigureMockMvc
class ProductControllerSpecification extends BaseControllerSpecification {

    @Autowired
    protected MockMvc mockMvc

    ProductService productService = Mock()

    @Subject
    ProductController productController = new ProductController(productService)

    def p0 = new Product()
    def p1 = new Product()
    def p2 = new Product()
    def productList = []

    def setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build()
        productList += [p0, p1, p2]
    }

    def "when the context is loaded then all expected beans are created" () {
        expect: "the productController and beans are created"
        mockMvc
    }

    def "should get all products and return http code 200" () {

        given:
        productService.findAll() >> productList

        when:
        def response = mockMvc.perform(get("/product/all"))

        then:
        1 * productService.findAll() >> productList
        response.andExpect(status().isOk())
    }

    def "should get empty products list and return http code 204" () {

        given:
        productService.findAll() >> []

        when:
        def response = mockMvc.perform(get("/product/all"))

        then:
        1 * productService.findAll() >> []
        response.andExpect(status().isNoContent())
    }

    def "should get all products created by given user and return http code 200" () {

        given:
        productService.findByCreatedBy("user") >> productList

        when:
        def response = mockMvc.perform(get("/product/all").param("createdBy", "user"))

        then:
        1 * productService.findByCreatedBy("user") >> productList
        response.andExpect(status().isOk())
    }

    def "should get empty products list created by given user and return http code 204" () {

        given:
        productService.findByCreatedBy("user") >> []

        when:
        def response = mockMvc.perform(get("/product/all").param("createdBy", "user"))

        then:
        1 * productService.findByCreatedBy("user") >> []
        response.andExpect(status().isNoContent())
    }

    def "should get not empty list and return http code 200" () {

        given:
        productService.findAllByActive(true) >> productList

        when:
        def response = mockMvc.perform(get("/product/all/activity").param("active", "true"))

        then:
        1 * productService.findAllByActive(true) >> productList
        response.andExpect(status().isOk())
    }

    def "should get empty list and return http code 204" () {

        given:
        productService.findAllByActive(true) >> []

        when:
        def response = mockMvc.perform(get("/product/all/activity").param("active", "true"))

        then:
        1 * productService.findAllByActive(true) >> []
        response.andExpect(status().isNoContent())
    }

    def "should get one object by id and return http code 200" () {

        given:
        productService.findById(1) >> Optional.ofNullable(p0)

        when:
        def response = mockMvc.perform(get("/product/one/1"))

        then:
        1 * productService.findById(1) >> Optional.ofNullable(p0)
        response.andExpect(status().isOk())
    }

    def "should create one object and return http code 200" () {

        given:
        ObjectMapper mapper = new ObjectMapper()
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false)
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter()
        def json = writer.writeValueAsString(p2)

        and:
        productService.create(p2) >> p2

        when:
        def response = mockMvc.perform(post("/product/create").contentType(MediaType.APPLICATION_JSON).content(json))

        then:
        1 * productService.create(p2) >> p2
        response.andExpect(status().isCreated())
    }

    def "should not create any object and return http code 400" () {

        when:
        def response = mockMvc.perform(post("/product/create"))

        then:
        response.andExpect(status().is4xxClientError())
    }

    def "should update existing product #p2 and return http code 200" () {

        given:
        ObjectMapper mapper = new ObjectMapper()
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false)
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter()
        def json = writer.writeValueAsString(p2)

        and:
        productService.findById(3) >> Optional.ofNullable(p2)

        and:
        productService.update(3, p2)

        when:
        def response = mockMvc.perform(put("/product/update/3").contentType(MediaType.APPLICATION_JSON).content(json))

        then:
        1 * productService.findById(3) >> Optional.ofNullable(p2)
        1 * productService.update(3, p2)
        response.andExpect(status().isOk())
    }

    def "should delete one product by given id and return http code 204" () {

        given:
        productService.deleteById(2)

        when:
        def response = mockMvc.perform(delete("/product/delete/2"))

        then:
        1 * productService.deleteById(2)
        response.andExpect(status().isNoContent())
    }

    def "should delete all products and return http code 210" () {

        given:
        productService.findAll() >> productList

        when:
        def response = mockMvc.perform(delete("/product/delete/all"))

        then:
        1 * productService.findAll() >> productList
        response.andExpect(status().isGone())
    }

    def "should try delete all products but the list is empty and return http code is 204" () {

        given:
        productService.findAll() >> []

        when:
        def response = mockMvc.perform(delete("/product/delete/all"))

        then:
        1 * productService.findAll() >> []
        response.andExpect(status().isNoContent())
    }
}
