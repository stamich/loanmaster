package pl.edu.ath.loanmaster.product.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.databind.SerializationFeature
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import pl.edu.ath.loanmaster.product.model.ExternalProduct
import pl.edu.ath.loanmaster.product.service.ExternalProductService

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@EnableWebMvc
@AutoConfigureMockMvc
class ExternalProductControllerSpecification extends BaseControllerSpecification {

    @Autowired
    protected MockMvc mockMvc

    ExternalProductService productService = Mock()

    ExternalProductController productController = new ExternalProductController(productService)

    def ep0 = new ExternalProduct()
    def ep1 = new ExternalProduct()
    def ep2 = new ExternalProduct()
    def epList = []

    def setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build()
        epList += [ep0, ep1, ep2]
    }

    def "should get all products and return http code 200" () {

        given:
        productService.findAll() >> epList

        when:
        def response = mockMvc.perform(get("/externalProduct/all"))

        then:
        1 * productService.findAll() >> epList
        response.andExpect(status().isOk())
    }

    def "should get not empty list and return http code 200" () {

        given:
        productService.findAllByActive(true) >> epList

        when:
        def response = mockMvc.perform(get("/externalProduct/all/activity").param("active", "true"))

        then:
        1 * productService.findAllByActive(true) >> epList
        response.andExpect(status().isOk())
    }

    def "should get empty list and return http code 204" () {

        given:
        productService.findAllByActive(true) >> []

        when:
        def response = mockMvc.perform(get("/externalProduct/all/activity").param("active", "true"))

        then:
        1 * productService.findAllByActive(true) >> []
        response.andExpect(status().isNoContent())
    }

    def "should get one object by id and return http code 200" () {

        given:
        productService.findById(2) >> Optional.ofNullable(ep0)

        when:
        def response = mockMvc.perform(get("/externalProduct/one/2"))

        then:
        1 * productService.findById(2) >> Optional.ofNullable(ep0)
        response.andExpect(status().isOk())
    }

    def "should create one object and return http code 200" () {

        given:
        ObjectMapper mapper = new ObjectMapper()
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false)
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter()
        def json = writer.writeValueAsString(ep2)

        and:
        productService.create(ep2) >> ep2

        when:
        def response = mockMvc.perform(post("/externalProduct/create").contentType(MediaType.APPLICATION_JSON).content(json))

        then:
        1 * productService.create(ep2) >> ep2
        response.andExpect(status().isCreated())
    }

    def "should not create any object and return http code 400" () {

        when:
        def response = mockMvc.perform(post("/externalProduct/create"))

        then:
        response.andExpect(status().is4xxClientError())
    }

    def "should update existing product #ep1 and return http code 200" () {

        given:
        ObjectMapper mapper = new ObjectMapper()
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false)
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter()
        def json = writer.writeValueAsString(ep1)

        and:
        productService.findById(2) >> Optional.ofNullable(ep1)

        and:
        productService.update(2, ep1)

        when:
        def response = mockMvc.perform(put("/externalProduct/update/2").contentType(MediaType.APPLICATION_JSON).content(json))

        then:
        1 * productService.findById(2) >> Optional.ofNullable(ep1)
        1 * productService.update(2, ep1)
        response.andExpect(status().isOk())
    }

    def "should delete one product by given id and return http code 204" () {

        given:
        productService.deleteById(2)

        when:
        def response = mockMvc.perform(delete("/externalProduct/delete/2"))

        then:
        1 * productService.deleteById(2)
        response.andExpect(status().isNoContent())
    }

    def "should delete all products and return http code 210" () {

        given:
        productService.findAll() >> epList

        when:
        def response = mockMvc.perform(delete("/externalProduct/delete/all"))

        then:
        1 * productService.findAll() >> epList
        response.andExpect(status().isGone())
    }

    def "should try delete all products but the list is empty and return http code is 204" () {

        given:
        productService.findAll() >> []

        when:
        def response = mockMvc.perform(delete("/externalProduct/delete/all"))

        then:
        1 * productService.findAll() >> []
        response.andExpect(status().isNoContent())
    }
}

