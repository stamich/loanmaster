package pl.edu.ath.loanmaster.product.service


import pl.edu.ath.loanmaster.product.model.Product
import pl.edu.ath.loanmaster.product.model.ProductDestination
import pl.edu.ath.loanmaster.product.model.ProductType
import pl.edu.ath.loanmaster.product.repository.ProductRepository
import spock.lang.Subject
import spock.lang.Unroll

import java.time.LocalDateTime

class ProductServiceSpecification extends BaseServiceSpecification {

    ProductRepository productRepository = Mock()

    @Subject
    ProductServiceImpl productService = new ProductServiceImpl(productRepository)

    def p0 = new Product()
    def p1 = new Product()
    def p2 = new Product()
    def productList = []

    def setup() {
        p0.setId(1)
        p0.setCreatedAt(LocalDateTime.now())
        p0.setCreatedBy("user")
        p0.setUpdatedAt(LocalDateTime.now())
        p0.setUpdatedBy("computer")
        p0.setName("Name 1")
        p0.setType(ProductType.PERSONAL_LOAN as Set<ProductType>)
        p0.setDestination(ProductDestination.INDIVIDUAL as Set<ProductDestination>)
        p0.setActive(true)

        p1.setId(2)
        p1.setCreatedAt(LocalDateTime.now())
        p1.setCreatedBy("admin")
        p1.setUpdatedAt(LocalDateTime.now())
        p1.setUpdatedBy("user")
        p1.setName("Name 2")
        p1.setType(ProductType.STUDENT_LOAN as Set<ProductType>)
        p1.setDestination(ProductDestination.INDIVIDUAL as Set<ProductDestination>)
        p1.setActive(true)

        p2.setId(3)
        p2.setCreatedAt(LocalDateTime.now())
        p2.setCreatedBy("user")
        p2.setUpdatedAt(LocalDateTime.now())
        p2.setUpdatedBy("user")
        p2.setName("Name 3")
        p2.setType(ProductType.MORTGAGE as Set<ProductType>)
        p2.setDestination(ProductDestination.CORPORATE as Set<ProductDestination>)
        p2.setActive(false)

        productList += [p0, p1, p2]
    }

    @Unroll
    def "should call once findAll() method from the repository" () {

        when:
        productService.findAll()

        then:
        1 * productRepository.findAll()
        0 * _
    }

    @Unroll
    def "should call once findAll() method and return the list of elements" () {

        given:
        1 * productRepository.findAll() >> productList

        when:
        def result = productService.findAll()

        then:
        result == productList
    }

    @Unroll
    def "should find all products created by given person" () {

        given:
        1 * productRepository.findByCreatedBy("user") >> [p0]

        when:
        def result = productService.findByCreatedBy("user")

        then:
        result == [p0]
    }

    @Unroll
    def "should find all active products" () {

        given:
        1 * productRepository.findAllByActive(true) >> [p0, p1]

        when:
        def result = productService.findAllByActive(true)

        then:
        result != [p0, p1, p2]
        result == [p0, p1]
    }

    @Unroll
    def "should find all product by its id" () {

        given:
        1 * productRepository.findById(2) >> Optional.ofNullable(p1)

        when:
        def result = productService.findById(2)

        then:
        result == Optional.ofNullable(p1)
    }

    @Unroll("The creating of product #p0")
    def "should add the element #p0" () {

        given:
        def p3 = new Product()
        p3.setId(4)
        p3.setCreatedAt(LocalDateTime.now())
        p3.setCreatedBy("user")
        p3.setUpdatedAt(LocalDateTime.now())
        p3.setUpdatedBy("user")
        p3.setName("Name 3")
        p3.setType(ProductType.MORTGAGE as Set<ProductType>)
        p3.setDestination(ProductDestination.CORPORATE as Set<ProductDestination>)
        p3.setActive(false)

        when:
        productService.create(p3)

        then:
        1 * productRepository.save(p3)
        0 * _
    }

    @Unroll
    def "should update existing product #p2" () {

        given:
        productRepository.findById(3) >> Optional.ofNullable(p2)
        productRepository.save(p2) >> p2

        when:
        productService.update(3, p2) >> p2

        then:
        productRepository.findById(3)
        p2.setName("Name 3 updated")
        productRepository.save(p2)
        p2.getName() == "Name 3 updated"
    }

    @Unroll
    def "should be called once deleteById() from repository" () {

        when:
        productService.deleteById(3)

        then:
        1 * productRepository.deleteById(3)
        0 * _
    }

    def "should be called once deleteAll() from repository" () {

        when:
        productService.deleteAll()

        then:
        1 * productRepository.deleteAll()
        0 * _
    }
}
