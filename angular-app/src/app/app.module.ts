import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { BoardAdminComponent } from './components/board-admin/board-admin.component';
import { BoardWorkerComponent } from './components/board-worker/board-worker.component';
import { BoardUserComponent } from './components/board-user/board-user.component';
import { CreditAddComponent } from './components/credit-add/credit-add.component';
import { CreditDetailsComponent } from './components/credit-details/credit-details.component';
import { CreditsListComponent } from './components/credits-list/credits-list.component';
import { OrderAddComponent } from './components/order-add/order-add.component';
import { OrderDetailsComponent } from './components/order-details/order-details.component';
import { OrdersListComponent } from './components/orders-list/orders-list.component';
import { ProductAddComponent } from './components/product-add/product-add.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { TaxAddComponent } from './components/tax-add/tax-add.component';
import { TaxDetailsComponent } from './components/tax-details/tax-details.component';
import { TaxesListComponent } from './components/taxes-list/taxes-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    BoardAdminComponent,
    BoardWorkerComponent,
    BoardUserComponent,
    CreditAddComponent,
    CreditDetailsComponent,
    CreditsListComponent,
    OrderAddComponent,
    OrderDetailsComponent,
    OrdersListComponent,
    ProductAddComponent,
    ProductDetailsComponent,
    ProductsListComponent,
    TaxAddComponent,
    TaxDetailsComponent,
    TaxesListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
