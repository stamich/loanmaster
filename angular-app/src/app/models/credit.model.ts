export class Credit {
  id?: any;
  personName?: string;
  personEmail?: string;
  salary?: bigint;
  creditAmount?: bigint;
  additionalFees?: string;
  monthlyWeights?: string;
  averageInstallment?: bigint;
  startDate?: number;
  endDate?: number;
  timeToPayOff?: number;
  creditWorthiness?: string;
}
