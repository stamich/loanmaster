export class User {
  id?: any;
  firstName?: string;
  lastName?: string;
  email?: string;
  username?: string;
  password?: string;
  clientTypes?: object;
  authorities?: object;
  accountNonExpired?: boolean;
  accountNonLocked?: boolean;
  credentialsNonExpired?: boolean;
  enabled?: boolean;
}
