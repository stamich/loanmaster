export class Product {
  id?: any;
  name?: string;
  destination?: object;
  type?: object;
  active?: boolean;
}
