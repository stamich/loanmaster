package pl.edu.ath.loanmaster.common.exception;

public class ObjectNotFoundException extends Exception {

    public ObjectNotFoundException(Long id) {
        super("Object with id: " + id + " was not found!");
    }
}
