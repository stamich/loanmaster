package pl.edu.ath.loanmaster.common.messaging;

public interface KafkaWrapper {

    /**
     * Returns unique id of object placed in the Kafka topic.
     * @return unique id
     */
    String getObjectId();

    /**
     * Returns the specific message from Kafka topic.
     * @return the message
     */
    String getMessage();

    /**
     * Returns the additional data from Kafka topic.
     * @return
     */
    String getAdditionalData();
}
